<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="33"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="40"/>
        <source>A todo and task managing application.</source>
        <translation>Une application de gestion des tâches et listes de tâches.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="68"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation>OpenTodoList est publié sous les termes de la &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 ou (à votre choix) toute version ultérieure.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="85"/>
        <source>Report an Issue</source>
        <translation>Signaler un problème</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="114"/>
        <source>Third Party Libraries and Resources</source>
        <translation>Bibliothèques et ressources tierces</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="151"/>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="156"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="163"/>
        <source>License:</source>
        <translation>Licence:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="168"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="174"/>
        <source>Download:</source>
        <translation>Télécharger :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="178"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="92"/>
        <source>Copy System Information</source>
        <translation>Copier les Informations Système</translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="20"/>
        <source>Select Account Type</source>
        <translation>Sélectionner type de compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="55"/>
        <source>Account Type</source>
        <translation>Type de compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="61"/>
        <source>NextCloud</source>
        <translation>NextCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="64"/>
        <source>ownCloud</source>
        <translation>ownCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="67"/>
        <source>WebDAV</source>
        <translation>WebDAV</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="70"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../qml/Pages/AccountsPage.qml" line="18"/>
        <location filename="../qml/Pages/AccountsPage.qml" line="40"/>
        <source>Accounts</source>
        <translation>Comptes</translation>
    </message>
</context>
<context>
    <name>AllSubtasksDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="13"/>
        <source>Everything in %1 done! Do you want to mark it as well as done?</source>
        <translation>Toutes les tâches dans %1 sont terminées ! Voulez-vous le marquer comme terminé ?</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation>Marquer comme terminer ?</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="30"/>
        <source>Keep Open</source>
        <translation>Laisser ouvert</translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="200"/>
        <source>Manage your personal data.</source>
        <translation>Gérer vos données personnelles.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="206"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation>Activer les optimisations pour écrans tactiles.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="209"/>
        <source>Only run the app background service</source>
        <translation>Exécuter uniquement le service d&apos;arrière-plan de l&apos;application</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="211"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation>Exécutez uniquement l&apos;interface graphique de l&apos;application et connectez-vous à un service d&apos;arrière-plan existant</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="216"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation>Activez une console sous Windows pour recueillir les données de débogage</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="367"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="433"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="375"/>
        <source>Quick Note</source>
        <translation>Note Rapide</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="219"/>
        <source>Background Sync</source>
        <translation>Synchronisation en arrière plan</translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="223"/>
        <source>App continues to sync your data in the background</source>
        <translation>L&apos;application poursuit la synchronisation de vos données en arrière plan</translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="227"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>ApplicationMenu</name>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="36"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="87"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="158"/>
        <source>Navigate</source>
        <translation>Naviguer</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="197"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="18"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="27"/>
        <source>About Qt</source>
        <translation>A propos de Qt</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="35"/>
        <source>Accounts</source>
        <translation>Comptes</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="43"/>
        <source>Add Tag</source>
        <translation>Ajouter un tag</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="51"/>
        <source>Attach File</source>
        <translation>Attacher un fichier</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="59"/>
        <source>Backup</source>
        <translation>Sauvegarde</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="66"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="73"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="81"/>
        <source>Copy Link To Page</source>
        <translation>Copier le lien vers la page</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="89"/>
        <source>Create Sample Library</source>
        <translation>Créer un librairie d&apos;exemple</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="159"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="167"/>
        <source>Delete Completed Items</source>
        <translation>Supprimer les points complétés</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="175"/>
        <source>Due Date</source>
        <translation>Échéance</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="183"/>
        <source>Find</source>
        <translation>Trouver</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="190"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="207"/>
        <source>Left Sidebar</source>
        <translation>Barre latérale gauche</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="213"/>
        <source>Mark all items as done</source>
        <translation>Marquer tout comme complété</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="225"/>
        <source>Mark all items as undone</source>
        <translation>Marquer tout comme non fait</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="237"/>
        <source>Move</source>
        <translation>Déplacer</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="245"/>
        <source>New Library</source>
        <translation>Nouvelle librairie</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="253"/>
        <source>Open Created Item</source>
        <translation>Ouvrir l&apos;objet créé </translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="260"/>
        <source>Open In New Window</source>
        <translation>Ouvrir dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="268"/>
        <source>Open Library Folder</source>
        <translation>Ouvrir le dossier de la libraire</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="274"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="280"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="288"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="309"/>
        <source>Scroll to Top</source>
        <translation>Aller au sommet</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="316"/>
        <source>Scroll to Bottom</source>
        <translation>Aller en bas</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="323"/>
        <source>Set Color</source>
        <translation>Définir la couleur</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="330"/>
        <source>Set Progress</source>
        <translation>Définir le progrès</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="337"/>
        <source>Sort</source>
        <translation>Trier</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="345"/>
        <source>Sync Now</source>
        <translation>Synchroniser maintenant</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="358"/>
        <source>Sync Log</source>
        <translation>Synchroniser le journal</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="369"/>
        <source>Translate The App...</source>
        <translation>Traduire l&apos;application...</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../qml/Components/ApplicationToolBar.qml" line="86"/>
        <source>Problems</source>
        <translation>Problèmes</translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="36"/>
        <source>Attach File</source>
        <translation>Joindre fichier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="51"/>
        <source>Delete Attachment?</source>
        <translation>Supprimer pièce jointe ?</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="55"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation>Êtes-vous sûr de vouloir supprimer la pièce jointe &lt;strong&gt;%1&lt;/strong&gt; ? Cette action ne peut pas être annulée.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="69"/>
        <source>Attachments</source>
        <translation>Fichiers joints</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation>Lilas</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation>Copie de</translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../qml/Actions/CopyTodo.qml" line="12"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../qml/Actions/CopyTopLevelItem.qml" line="12"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
</context>
<context>
    <name>DeepLinkHandler</name>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="56"/>
        <source>Uuups... seems that&apos;s a dead end...</source>
        <translation>Ouuups... le lien semble rompu...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="65"/>
        <source>Sorry, that link does not lead to any page or item that is present on this device. Check if the library or item to which the link points is synchronized on this device and try again.</source>
        <translation>Désolé, ce lien mène à aucune page ou élément présent sur cet appareil. Vérifiez que la bibliothèque ou l&apos;élément est bien synchronisé sur cet appareil et réessayez.</translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="18"/>
        <source>Delete Account?</source>
        <translation>Supprimer le compte ?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="35"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer le compte &lt;strong&gt;%1&lt;/strong&gt; ? Cela supprimera toutes les bibliothèques appartenant au compte de votre appareil?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note : Vous pouvez les restaurer à partir du serveur en réajoutant le compte.&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../qml/Actions/DeleteCompletedChildren.qml" line="10"/>
        <source>Delete Completed Items</source>
        <translation>Effacer les éléments terminés</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="18"/>
        <source>Delete Completed Items?</source>
        <translation>Effacer les éléments terminé ?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Voulez-vous vraiment effacer les todos de la liste &lt;strong&gt;%1&lt;/strong&gt;? Ceci ne pourra pas être annulé.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="43"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Voulez-vous vraiment effacer les tâches de la liste &lt;strong&gt;%1&lt;/strong&gt;? Ceci ne pourra pas être annulé. </translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../qml/Actions/DeleteItem.qml" line="10"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="18"/>
        <source>Delete Item?</source>
        <translation>Supprimer l&apos;élément ?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="31"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Souhaitez-vous vraiment supprimer l&apos;image &lt;strong&gt;%1&lt;/strong&gt; ? Cela ne peut pas être annulé.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="35"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Souhaitez-vous vraiment supprimer la liste des tâches &lt;strong&gt;%1&lt;/solide&gt; ? Cela ne peut pas être annulé.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Voulez-vous vraiment supprimer le todo &lt;strong&gt;%1&lt;/strong&gt; ? Cela ne peut pas être annulé.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="43"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Souhaitez-vous vraiment supprimer la tâche &lt;strong&gt;%1&lt;/strong&gt; ? Cela ne peut pas être annulé.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="46"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Souhaitez-vous vraiment supprimer la note &lt;strong&gt;%1&lt;/strong&gt; ? Cela ne peut pas être annulé.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="49"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Souhaitez-vous vraiment supprimer la page &lt;forte&gt;%1&lt;/strong&gt; ? C&apos;est impossible à annuler.</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="18"/>
        <source>Delete Library?</source>
        <translation>Supprimer la bibliothèque ?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="32"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer la bibliothèque &lt;strong&gt;%1&lt;/strong&gt; de l&apos;application ? &lt;em&gt;Cela supprimera tous les fichiers appartenant à la bibliothèque.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="38"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation>Souhaitez-vous vraiment supprimer la bibliothèque &lt;strong&gt;%1&lt;/strong&gt; de l&apos;application ? Notez que les fichiers à l&apos;intérieur de la bibliothèque ne seront pas supprimés, vous pourrez donc restaurer la bibliothèque ultérieurement.</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="26"/>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="65"/>
        <source>Connection Settings</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="73"/>
        <source>Trouble Signing In?</source>
        <translation>Des difficultés pour s&apos;enregistrer ?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="84"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation>Nous avons tenté d&apos;ouvrir le navigateur pour vour vous connectez à Dropbox. Veuillez vous connecter et autoriser l&apos;accès à OpenTodoList.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="91"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation>Votre navigateur ne s&apos;est pas ouvert ? Vous essayer de l&apos;ouvrir à nouveau ou copiez l&apos;URL requise manuellement dans votre navigateur.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="95"/>
        <source>Authorize...</source>
        <translation>Autorisation...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="104"/>
        <source>Open Browser</source>
        <translation>Ouvrir le navigateur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="110"/>
        <source>Copy Link</source>
        <translation>Copier le lien</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="114"/>
        <source>Copied!</source>
        <translation>Copié !</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="123"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="130"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="22"/>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="57"/>
        <source>Edit Account</source>
        <translation>Editer Compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="63"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="74"/>
        <source>Server Address:</source>
        <translation>Adresse du serveur :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="81"/>
        <source>https://myserver.example.com</source>
        <translation>https://serveur.exemple.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="89"/>
        <source>Login</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="102"/>
        <source>User:</source>
        <translation>Utilisateur :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="109"/>
        <source>User Name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="115"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="122"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="131"/>
        <source>Disable Certificate Checks</source>
        <translation>Désactiver le contrôle de certificat</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="140"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Impossible de se connecter au serveur. Svp vérifiez votre nom d&apos;utilisateur, mot de passe et l&apos;adresse du serveur ; et retentez.</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="32"/>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="116"/>
        <source>Edit Account</source>
        <translation>Modifier le compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="122"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="133"/>
        <source>Server Address:</source>
        <translation>Adresse du serveur : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="139"/>
        <source>https://myserver.example.com</source>
        <translation>https://serveur.exemple.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="146"/>
        <source>User:</source>
        <translation>Utilisateur :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="152"/>
        <source>User Name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="159"/>
        <source>Password:</source>
        <translation>Mot de passe : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="166"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="176"/>
        <source>Disable Certificate Checks</source>
        <translation>Annuler le contrôle de certificat</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="184"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>La connexion au serveur a échoué. Veuillez vérifier votre nom d&apos;utilisateur, votre mot de passe ainsi que l&apos;adresse du serveur et réessayer.</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="68"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt; a été créé.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="74"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="79"/>
        <source>Dismiss</source>
        <translation>Masquer</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="31"/>
        <source>Due on</source>
        <translation>Pour le</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="69"/>
        <source>First due on %1.</source>
        <translation>Première échéance le %1.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="88"/>
        <source>No recurrence pattern set...</source>
        <translation>Pas de répétition programmée...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="90"/>
        <source>Recurs every day.</source>
        <translation>Se répète chaque jour</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="92"/>
        <source>Recurs every week.</source>
        <translation>Se répète chaque semaine</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="94"/>
        <source>Recurs every month.</source>
        <translation>Se répète chaque mois</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="98"/>
        <source>Recurs every %1 days.</source>
        <translation>Se répète chaque %1 jour(s).</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="96"/>
        <source>Recurs every year.</source>
        <translation>Se répète chaque année.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="101"/>
        <source>Recurs every %1 weeks.</source>
        <translation>Se répète chaque %1 semaines.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="104"/>
        <source>Recurs every %1 months.</source>
        <translation>Se répète chaque %1 mois.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="141"/>
        <source>Recurs until %1.</source>
        <translation>Répéter jusq&apos;au %1</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="144"/>
        <source>Recurs indefinitely</source>
        <translation>Répéter indéfiniment</translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="28"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="73"/>
        <source>No notes added yet - click here to add some.</source>
        <translation>Aucune note n&apos;a encore été ajoutée - cliquez ici pour en ajouter.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="160"/>
        <source>Export to File...</source>
        <translation>Exporter vers le fichier...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>Markdown files</source>
        <translation>Fichiers Markdown</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="106"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="113"/>
        <source>Copy Formatted Text</source>
        <translation>Copier le texte formatté</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="129"/>
        <source>Copy Plain Text</source>
        <translation>Copier le texte brut</translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="190"/>
        <source>Move Todo Into...</source>
        <translation>Déplacer ToDo dans ...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="220"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation>Convertir une tâche en Todo et la déplacer dans...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="236"/>
        <source>Copy Item Into...</source>
        <translation>Copier l&apos;élément dans...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="253"/>
        <source>Copy Todo Into...</source>
        <translation>Copier la tâche dans...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="205"/>
        <source>Move Task Into...</source>
        <translation>Déplacer la tâche vers...</translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="55"/>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="223"/>
        <source>Schedule</source>
        <translation>Calendrier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="85"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="78"/>
        <source>Edit List</source>
        <translation>Editer la liste</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Hide Schedule</source>
        <translation>Cacher le calendrier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Show Schedule</source>
        <translation>Voir le calendrier</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="201"/>
        <source>Move Up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="210"/>
        <source>Move Down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="93"/>
        <source>Donate</source>
        <translation>Donner</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="293"/>
        <source>Untagged</source>
        <translation>Non taggué</translation>
    </message>
</context>
<context>
    <name>Library</name>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="615"/>
        <source>Unable to create backup folder</source>
        <translation>Impossible de créer un dossier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="619"/>
        <source>Failed to change into backup folder</source>
        <translation>Échec de conversion vers un dossier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="630"/>
        <source>Failed to open file for writing: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier en écriture: %1</translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="210"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="217"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="224"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="231"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="238"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="245"/>
        <source>Lilac</source>
        <translation>Lilas</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="252"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="262"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="273"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="355"/>
        <source>Note Title</source>
        <translation>Titre de la note</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="368"/>
        <source>Todo List Title</source>
        <translation>Titre de la todo list</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="381"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Terme de recherche 1, terme de recherche 2, ....</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="458"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation>Rien pour l&apos;instant ! Commencez par ajouter une &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; ou &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="514"/>
        <source>Sort By</source>
        <translation>Trier par</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="518"/>
        <source>Manually</source>
        <translation>Manuellement</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="525"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="532"/>
        <source>Due To</source>
        <translation>Échéance</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="539"/>
        <source>Created At</source>
        <translation>Créé à</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="546"/>
        <source>Updated At</source>
        <translation>Mis à jour à</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="267"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="285"/>
        <source>Select Image</source>
        <translation>Choisir une image</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="13"/>
        <source>Synchronization Log</source>
        <translation>Journal de synchronisation</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="68"/>
        <source>Debugging information</source>
        <translation>Informations de debug</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="70"/>
        <source>Warning</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="72"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="74"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="76"/>
        <source>Upload</source>
        <translation>Téléverser</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="78"/>
        <source>Create local folder</source>
        <translation>Créer un dossier local</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="80"/>
        <source>Create remote folder</source>
        <translation>Créer un dossier distant</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="82"/>
        <source>Deleting locally</source>
        <translation>Suppression de l&apos;élément locale</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="84"/>
        <source>Deleting remotely</source>
        <translation>Suppression de l&apos;élément distant</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="86"/>
        <source>Unknown log message type</source>
        <translation>Type de message de debug inconnu</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="23"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="197"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation>Commencez par &lt;a href=&apos;#newLibrary&apos;&gt;créer une nouvelle bibliothèque&lt;/a&gt;. Les bibliothèques sont utilisées pour stocker différents types d&apos;éléments comme des notes, des listes de tâches et des images.</translation>
    </message>
</context>
<context>
    <name>MarkFutureInstanceAsDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="13"/>
        <source>%1 is scheduled for the future - do you want to mark that future instance as done?</source>
        <translation>%1 est marqué pour l&apos;avenir. Voulez-vous marquer les instances futures comme terminées ?</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation>Marquer comme complété</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="29"/>
        <source>Keep Open</source>
        <translation>Laisser ouvert</translation>
    </message>
</context>
<context>
    <name>MoveTask</name>
    <message>
        <location filename="../qml/Actions/MoveTask.qml" line="12"/>
        <source>Move</source>
        <translation>Déplacer</translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../qml/Actions/MoveTodo.qml" line="12"/>
        <source>Move</source>
        <translation>Déplacer</translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="53"/>
        <source>Connection Settings</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="61"/>
        <source>Trouble Signing In?</source>
        <translation>Des difficultés pour s&apos;enregistrer ?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="72"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation>Nous avons tenté d&apos;ouvrir le navigateur pour vour vous connectez à Dropbox. Veuillez vous connecter et autoriser l&apos;accès à OpenTodoList.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="79"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation>Votre navigateur ne s&apos;est pas ouvert  ? Réessayez ou copiez l&apos;URL vers la barre de votre navigateur.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="85"/>
        <source>Authorize...</source>
        <translation>Autorisation...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="94"/>
        <source>Open Browser</source>
        <translation>Ouvrir le navigateur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="100"/>
        <source>Copy Link</source>
        <translation>Copier le lien</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="104"/>
        <source>Copied!</source>
        <translation>Copié !</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="113"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="120"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="104"/>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="105"/>
        <source>Tomorrow</source>
        <translation>Demain</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="106"/>
        <source>This Week</source>
        <translation>Cette semaine</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="107"/>
        <source>Next Week</source>
        <translation>Semaine prochaine</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="108"/>
        <source>Select...</source>
        <translation>Choisir...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="138"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="143"/>
        <source>The title for your new item...</source>
        <translation>Titre de votre nouvel élément...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="185"/>
        <source>Create in:</source>
        <translation>Créé dans :</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="226"/>
        <source>Due on:</source>
        <translation>Pour le :</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="149"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="81"/>
        <source>Create Library in Account</source>
        <translation>Créer une bibliothèque dans le compte </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="90"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation>Une bibliothèque créée dans un compte est synchronisée avec celui-ci. Cela permet de sauvegarder facilement une bibliothèque sur un serveur et de la restaurer plus tard à partir de là. En outre, ces bibliothèques peuvent être partagées avec d&apos;autres utilisateurs (si le serveur le permet).</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="98"/>
        <source>Existing Libraries</source>
        <translation>Bibliothèques existantes</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="106"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation>Sélectionner une bibliothèque existante pour l&apos;ajouter à l&apos;application</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="112"/>
        <source>No libraries were found on the server.</source>
        <translation>Aucune bibliothèque trouvée sur le serveur.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="121"/>
        <source>Searching existing libraries...</source>
        <translation>Recherche des bibliothèques existantes</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="164"/>
        <source>Create a New Library</source>
        <translation>Créer une nouvelle bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="172"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation>Créez une nouvelle bibliothèque, qui sera synchronisée avec le serveur. Une telle bibliothèque peut être ajoutée à l&apos;application sur d&apos;autres appareils pour synchroniser les données.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="186"/>
        <source>My new library&apos;s name</source>
        <translation>Le nom de ma bibliothèque
</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="100"/>
        <source>Open a Folder as a Library</source>
        <translation>Ouvrir un dossier comme bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="108"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation>Vous pouvez utiliser n&apos;importe quel dossier comme emplacement pour une bibliothèque.&lt;br/&gt;&lt;br/&gt;Ceci est particulièrement utile lorsque vous souhaitez utiliser un autre outil (comme un client de synchronisation d&apos;un fournisseur de cloud) pour synchroniser vos données avec un serveur. </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="112"/>
        <source>Folder:</source>
        <translation>Dossier : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="117"/>
        <source>Path to a folder to use as a library</source>
        <translation>Chemin vers le dossier à utiliser comme bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="123"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="128"/>
        <source>Name:</source>
        <translation>Nom : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="133"/>
        <source>My Local Library Name</source>
        <translation>Nom de ma bibliothèque locale</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation>Choisir un dossier</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="22"/>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="62"/>
        <source>Create Library</source>
        <translation>Créer une bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="69"/>
        <source>Local Library</source>
        <translation>Bibliothèque locale</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="76"/>
        <source>Use Folder as Library</source>
        <translation>Utiliser le dossier comme bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="88"/>
        <source>Add Libraries From Your Accounts</source>
        <translation>Ajouter les bibliothèques de vos comptes</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="115"/>
        <source>Add Account</source>
        <translation>Ajouter un compte</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="61"/>
        <source>Create a Local Library</source>
        <translation>Créer une bibliothèque locale</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="69"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation>Une bibliothèque locale est stockée uniquement sur votre appareil - ce qui la rend parfaitement conciliante de la vie privée !&lt;br/&gt;&lt;br/&gt;Utilisez-la lorsque vous souhaitez stocker des informations uniquement localement et sauvegarder régulièrement toutes vos données via d&apos;autres mécanismes. Si vous devez accéder à vos informations sur plusieurs appareils, créez plutôt une bibliothèque qui sera synchronisée.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="79"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="84"/>
        <source>My Local Library Name</source>
        <translation>Nom de ma bibliothèque locale</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="19"/>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="71"/>
        <source>Connection Settings</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="77"/>
        <source>Server Address:</source>
        <translation>Adresse Serveur :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="107"/>
        <source>Trouble Signing In?</source>
        <translation>Problème pour s&apos;identifier ?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="118"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation>Nous avons tenté d&apos;ouvrir une page de connexion Nextcloud dans votre navigateur. Svp, connectez vous et autorisez OpenTodoList à accéder à votre compte pour pouvoir continuer. En cas de problème pour accéder à Nextcloud dans le navigateur, vous pouvez entrer manuellement votre identifiant et mot de passe.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="126"/>
        <source>Log in Manually</source>
        <translation>Connexion manuelle</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="132"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation>Idéalement, vous utilisez des mots de passe spécifiques aux applications au lieu de votre mot de passe utilisateur. Si votre connexion est protégée par l&apos;authentification à 2 facteurs (2FA), vous devez même utiliser des mots de passe spécifiques à l&apos;application pour accéder à votre NextCloud. Vous pouvez créer de tels mots de passe dans vos paramètres d&apos;utilisateur. </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="141"/>
        <source>Create App Password</source>
        <translation>Créer un mot de passe d&apos;application</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="150"/>
        <source>Account Settings</source>
        <translation>Paramètres du compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="160"/>
        <source>Copy Link</source>
        <translation>Copier le lien</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="166"/>
        <source>Copied!</source>
        <translation>Copié !</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="175"/>
        <source>User:</source>
        <translation>Utilisateur :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="182"/>
        <source>User Name</source>
        <translation>Nom d&apos;utilisateur : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="189"/>
        <source>Password:</source>
        <translation>Mot de passe : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="196"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="206"/>
        <source>Disable Certificate Checks</source>
        <translation>Désactiver le contrôle de certificat</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="210"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="223"/>
        <source>Account Name</source>
        <translation>Nom de Compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="233"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>La connexion au serveur a échoué. Veuillez vérifier votre nom d&apos;utilisateur, votre mot de passe ainsi que l&apos;adresse du serveur et réessayer.</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="52"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="41"/>
        <source>Todo List</source>
        <translation>Liste des tâches</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="56"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="46"/>
        <source>Todo</source>
        <translation>Todo</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="161"/>
        <source>Account Name</source>
        <translation>Nom du compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="28"/>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="99"/>
        <source>Connection Settings</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="105"/>
        <source>Server Address:</source>
        <translation>Adresse du serveur : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="111"/>
        <source>https://myserver.example.com</source>
        <translation>https://serveur.exemple.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="117"/>
        <source>User:</source>
        <translation>Utilisateur : </translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="123"/>
        <source>User Name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="129"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="135"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="144"/>
        <source>Disable Certificate Checks</source>
        <translation>Annuler le contrôle de certificat</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="148"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="171"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>La connexion au serveur a échoué. Veuillez vérifier votre nom d&apos;utilisateur, votre mot de passe ainsi que l&apos;adresse du serveur et réessayer.</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../qml/Widgets/NoteItem.qml" line="91"/>
        <source>Due on %1</source>
        <translation>Pour le %1</translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="171"/>
        <source>Main Page</source>
        <translation>Page principale</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="238"/>
        <source>New Page</source>
        <translation>Nouvelle page</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation>Langue du système</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation>Données d&apos;accès manquantes pour le compte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="18"/>
        <location filename="../qml/Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation>Problème détecté</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation>La synchronisation a échoué pour la bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="91"/>
        <source>Retry Sync</source>
        <translation>Relancer la synchro</translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../qml/Actions/PromoteTask.qml" line="13"/>
        <source>Promote</source>
        <translation>Promouvoir</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="312"/>
        <location filename="../appstartup.cpp" line="319"/>
        <source>unknown</source>
        <translation>inconnu</translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="17"/>
        <source>Quick Notes</source>
        <translation>Notes Rapides</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="36"/>
        <source>Quick Notes Editor</source>
        <translation>Éditeur de Notes Rapides</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="45"/>
        <source>Open the main window</source>
        <translation>Ouvrir la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="57"/>
        <source>Quick Note Title</source>
        <translation>Titre de la Note Rapide</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="121"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="126"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation>Sauvegarder les notes saisies dans la bibliothèque sélectionnée. Maintenez le bouton enfoncé pour obtenir d&apos;autres options d&apos;enregistrement.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="148"/>
        <source>Save as Note</source>
        <translation>Sauvegarder comme Note</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="161"/>
        <source>Save as Todo List</source>
        <translation>Sauvegarder comme Liste des Tâches</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="165"/>
        <source>Quick Todo List</source>
        <translation>Liste des Tâches Rapide</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="152"/>
        <source>Quick Note</source>
        <translation>Note Rapide</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="70"/>
        <source>Type your notes here...</source>
        <translation>Saisissez vos notes ici...</translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="29"/>
        <source>Edit Recurrence</source>
        <translation>Modifier la répétition</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="47"/>
        <source>Daily</source>
        <translation>Journalier</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="51"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="55"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="63"/>
        <source>Every N Days</source>
        <translation>Chaque N jour(s)</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="97"/>
        <source>Recurs:</source>
        <translation>Se répète :</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="115"/>
        <source>Number of days:</source>
        <translation>Nombre de jours :</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="141"/>
        <source>Recur relative to the date when marking as done</source>
        <translation>Se répète une fois la tâche réalisée</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="59"/>
        <source>Yearly</source>
        <translation>Annuellement</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="67"/>
        <source>Every N Weeks</source>
        <translation>Toutes les N Semaines</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="71"/>
        <source>Every N Months</source>
        <translation>Tous les N Mois</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="117"/>
        <source>Number of weeks:</source>
        <translation>Nombre de semaines</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="119"/>
        <source>Number of months:</source>
        <translation>Nombre de mois</translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../qml/Actions/RenameItem.qml" line="10"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="21"/>
        <source>Rename Item</source>
        <translation>Renommer l&apos;élément</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="32"/>
        <source>Enter item title...</source>
        <translation>Saisir le titre de l&apos;élément...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="19"/>
        <source>Rename Library</source>
        <translation>Renommer la bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="36"/>
        <source>Enter library title...</source>
        <translation>Entrer le titre de la bibliothèque...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../qml/Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation>Réinitialiser l&apos;échéance</translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="147"/>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="148"/>
        <source>Tomorrow</source>
        <translation>Demain</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="184"/>
        <source>Later This Week</source>
        <translation>Plus tard cette semaine</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="186"/>
        <source>Next Week</source>
        <translation>Semaine prochaine</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="187"/>
        <source>Coming Next</source>
        <translation>Prochainement</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="266"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation>Rien de prévu... Ajoutez une date d&apos;échéance aux éléments pour qu&apos;ils apparaissent ici.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="123"/>
        <source>Overdue</source>
        <translation>En retard</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="52"/>
        <source>Schedule</source>
        <translation>Planifier</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/SelectLibraryDialog.qml" line="21"/>
        <source>Select Library</source>
        <translation>Choisir Bibliothèque</translation>
    </message>
</context>
<context>
    <name>SelectTodoDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="22"/>
        <source>Select Todo</source>
        <translation>Sélectionner le Todo</translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="42"/>
        <source>Todo List:</source>
        <translation>Liste des tâches:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="64"/>
        <source>Todo:</source>
        <translation>Tâches:</translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoListDialog.qml" line="24"/>
        <source>Select Todo List</source>
        <translation>Sélectionnez la Todo Liste</translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTopLevelItemDialog.qml" line="21"/>
        <source>Select Item</source>
        <translation>Sélectionner un élément</translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation>Pour cette semaine</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation>Pour la semaine prochaine</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../qml/Actions/SetDueTo.qml" line="10"/>
        <source>Select Due Date</source>
        <translation>Choisir la date d&apos;échéance</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../qml/Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation>Pour aujourd&apos;hui</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../qml/Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation>Pour demain</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../qml/Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation>Définir l&apos;avancement</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="46"/>
        <source>User Interface</source>
        <translation>Interface utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="54"/>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="118"/>
        <source>Theme:</source>
        <translation>Thème :</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="208"/>
        <source>Font Size:</source>
        <translation>Taille de la fonte</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="225"/>
        <source>Use custom font size</source>
        <translation>Utiliser une taille de police personnalisée</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="243"/>
        <source>Use Compact Style</source>
        <translation>Utiliser un style compact</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="252"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation>Réduire l&apos;espace entre les composants et réduire la taille de la police.

&lt;em&gt;Nécessite un redémarrage de l&apos;application.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="260"/>
        <source>Use compact todo lists</source>
        <translation>Utiliser des listes des tâches compactes</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="270"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation>Réduire l&apos;écart de remplissage dans les listes de tâches et d&apos;actions pour faire tenir plus d&apos;éléments à l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="277"/>
        <source>Override Scaling Factor</source>
        <translation>Remplacer le facteur d&apos;échelle</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="284"/>
        <source>Scale Factor:</source>
        <translation>Facteur d&apos;échelle:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="304"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation>Utilisez cette option pour adapter manuellement l&apos;interface utilisateur. Par défaut, l&apos;application devrait s&apos;adapter automatiquement à la configuration de votre appareil. Si cela ne fonctionne pas correctement, vous pouvez définir un facteur d&apos;échelle personnalisé ici.

Cela nécessite un redémarrage de l&apos;application.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="313"/>
        <source>Library Item Size:</source>
        <translation>Taille des Objets de la Bibliothèque:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="189"/>
        <source>System Tray:</source>
        <translation>Barre des Tâches:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="202"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation>Ouvrir l’Éditeur de Notes Rapides au Clic</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="234"/>
        <source>Desktop Mode</source>
        <translation>Mode bureau</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="329"/>
        <source>Show notes excerpt in listings</source>
        <translation>Montrer un extrait des notes dans les listes</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="194"/>
        <source>Monochrome Icon</source>
        <translation>Icône monochrome</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="143"/>
        <source>Custom Primary Color:</source>
        <translation>Couleur primaire personnalisée:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Pages/SettingsPage.qml" line="171"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="165"/>
        <source>Custom Secondary Color:</source>
        <translation>Couleur personnalisée secondaire:</translation>
    </message>
</context>
<context>
    <name>StackViewWindow</name>
    <message>
        <location filename="../qml/Windows/StackViewWindow.qml" line="21"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="45"/>
        <source>Libraries</source>
        <translation>Bibliothèques</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="70"/>
        <source>Add a new library</source>
        <translation>Ajouter une nouvelle bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="89"/>
        <source>Accounts</source>
        <translation>Comptes</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="111"/>
        <source>Add an account</source>
        <translation>Ajouter un compte</translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="42"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation>Il y a eu des erreurs lors de la synchronisation de la bibliothèque. Veuillez vous assurer que les paramètres de la bibliothèque sont à jour.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="48"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="52"/>
        <source>View</source>
        <translation>Afficher</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../qml/Widgets/TagsEditor.qml" line="32"/>
        <source>Add Tag</source>
        <translation>Ajouter un tag</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="136"/>
        <source>✔ No open todos - everything done</source>
        <translation>✔ Pas de Todo en cours - tout est fait</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="77"/>
        <source>Due on %1</source>
        <translation>Pour le %1</translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="166"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Terme de recherche 1, terme de recherche 2, ....</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="253"/>
        <source>Todos</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="273"/>
        <source>Add new todo...</source>
        <translation>Ajouter un nouveau todo...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="178"/>
        <source>Manually</source>
        <translation>Manuellement</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="184"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="190"/>
        <source>Due Date</source>
        <translation>Date d&apos;échéance</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="196"/>
        <source>Created At</source>
        <translation>Créé à</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="202"/>
        <source>Updated At</source>
        <translation>Mis à jour à</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="215"/>
        <source>Show Completed</source>
        <translation>Montrer les terminés</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="223"/>
        <source>Show At The End</source>
        <translation>Afficher à la fin</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="181"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Terme de recherche 1, terme de recherche 2, ....</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="206"/>
        <source>Tasks</source>
        <translation>Tâches</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="212"/>
        <source>Add new task...</source>
        <translation>Ajouter une nouvelle tâche...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="303"/>
        <source>Show Completed</source>
        <translation>Montrer les terminés</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="310"/>
        <source>Show At The End</source>
        <translation>Afficher à la fin</translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../qml/Widgets/TodosWidget.qml" line="211"/>
        <source>Due on: %1</source>
        <translation>Pour le: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../qml/Widgets/TodosWidgetDelegate.qml" line="238"/>
        <source>More Actions...</source>
        <translation>Autres actions...</translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation>Une mise à jour d&apos;OpenTodoList %1 est disponible.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
</context>
</TS>
