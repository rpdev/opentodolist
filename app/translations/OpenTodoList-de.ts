<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation>Über …</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="33"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="40"/>
        <source>A todo and task managing application.</source>
        <translation>Eine Anwendung zur Aufgabenplanung und für Notizen.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="68"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation>OpenTodoList wird unter den Bedingungen der &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; Version 3 oder jeder späteren Version veröffentlicht.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="85"/>
        <source>Report an Issue</source>
        <translation>Ein Problem melden</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="114"/>
        <source>Third Party Libraries and Resources</source>
        <translation>Bibliotheken und Ressourcen von Drittanbietern</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="151"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="156"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="163"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="168"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="174"/>
        <source>Download:</source>
        <translation>Herunterladen:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="178"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="92"/>
        <source>Copy System Information</source>
        <translation>Kopiere Systeminformationen</translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="20"/>
        <source>Select Account Type</source>
        <translation>Konto-Typ auswählen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="55"/>
        <source>Account Type</source>
        <translation>Konto-Typ</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="61"/>
        <source>NextCloud</source>
        <translation>NextCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="64"/>
        <source>ownCloud</source>
        <translation>ownCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="67"/>
        <source>WebDAV</source>
        <translation>WebDAV</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="70"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../qml/Pages/AccountsPage.qml" line="18"/>
        <location filename="../qml/Pages/AccountsPage.qml" line="40"/>
        <source>Accounts</source>
        <translation>Konten</translation>
    </message>
</context>
<context>
    <name>AllSubtasksDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="13"/>
        <source>Everything in %1 done! Do you want to mark it as well as done?</source>
        <translation>Alles in %1 erledigt! Möchten Sie es auch als erledigt markieren?</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation>Als erledigt markieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="30"/>
        <source>Keep Open</source>
        <translation>Geöffnet halten</translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="200"/>
        <source>Manage your personal data.</source>
        <translation>Verwalte deine persönlichen Daten.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="206"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation>Optimierungen für Touchscreens anschalten</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="209"/>
        <source>Only run the app background service</source>
        <translation>Nur den Hintergrundservice der Anwendung starten</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="211"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation>Nur Anwendungs-GUI starten und mit einem existierenden Hintergrunddienst verbinden</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="216"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation>Unter Windows eine Konsole zum Sammeln von Debug-Ausgaben aktivieren</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="367"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="433"/>
        <source>Quit</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="375"/>
        <source>Quick Note</source>
        <translation>Kurznotiz</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="219"/>
        <source>Background Sync</source>
        <translation>Hintergrund Synchronisation</translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="223"/>
        <source>App continues to sync your data in the background</source>
        <translation>Die App synchronisiert Ihre Daten weiterhin im Hintergrund</translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="227"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>ApplicationMenu</name>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="36"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="87"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="158"/>
        <source>Navigate</source>
        <translation>Navigieren</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="197"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="18"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="27"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="35"/>
        <source>Accounts</source>
        <translation>Konten</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="43"/>
        <source>Add Tag</source>
        <translation>Tag hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="51"/>
        <source>Attach File</source>
        <translation>Datei anhängen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="59"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="66"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="73"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="81"/>
        <source>Copy Link To Page</source>
        <translation>Link auf Seite kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="89"/>
        <source>Create Sample Library</source>
        <translation>Beispielbibliothek erzeugen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="159"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="167"/>
        <source>Delete Completed Items</source>
        <translation>Erledigte Einträge löschen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="175"/>
        <source>Due Date</source>
        <translation>Fälligkeitsdatum</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="183"/>
        <source>Find</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="190"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="207"/>
        <source>Left Sidebar</source>
        <translation>Linke Seitenleiste</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="213"/>
        <source>Mark all items as done</source>
        <translation>Alle Einträge als erledigt markieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="225"/>
        <source>Mark all items as undone</source>
        <translation>Alle Einträge als unerledigt markieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="237"/>
        <source>Move</source>
        <translation>Verschieben</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="245"/>
        <source>New Library</source>
        <translation>Neue Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="253"/>
        <source>Open Created Item</source>
        <translation>Angelegten Eintrag öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="260"/>
        <source>Open In New Window</source>
        <translation>In neuem Fenster öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="268"/>
        <source>Open Library Folder</source>
        <translation>Bibliotheksordner öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="274"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="280"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="288"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="309"/>
        <source>Scroll to Top</source>
        <translation>An den Anfang scrollen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="316"/>
        <source>Scroll to Bottom</source>
        <translation>Ans Ende scrollen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="323"/>
        <source>Set Color</source>
        <translation>Farbe setzen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="330"/>
        <source>Set Progress</source>
        <translation>Fortschritt setzen</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="337"/>
        <source>Sort</source>
        <translation>Sortieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="345"/>
        <source>Sync Now</source>
        <translation>Jetzt synchronisieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="358"/>
        <source>Sync Log</source>
        <translation>Synch Log</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="369"/>
        <source>Translate The App...</source>
        <translation>Die App übersetzen …</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../qml/Components/ApplicationToolBar.qml" line="86"/>
        <source>Problems</source>
        <translation>Probleme</translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="36"/>
        <source>Attach File</source>
        <translation>Datei anhängen</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="51"/>
        <source>Delete Attachment?</source>
        <translation>Anhang löschen?</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="55"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation>Soll der Anhang &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="69"/>
        <source>Attachments</source>
        <translation>Anhänge</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation>Weiß</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation>Gelb</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation>Lila</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation>Kopie von</translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../qml/Actions/CopyTodo.qml" line="12"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../qml/Actions/CopyTopLevelItem.qml" line="12"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
</context>
<context>
    <name>DeepLinkHandler</name>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="56"/>
        <source>Uuups... seems that&apos;s a dead end...</source>
        <translation>Hoppla … es sieht so aus, als ob es hier nicht weitergeht …</translation>
    </message>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="65"/>
        <source>Sorry, that link does not lead to any page or item that is present on this device. Check if the library or item to which the link points is synchronized on this device and try again.</source>
        <translation>Entschuldigung, diese Verknüpfung verweist auf keine Seite oder keinen Eintrag, der auf diesem Gerät vorhanden ist. Bitte überprüfen Sie, ob die Bibliothek oder der Eintrag, auf die oder den die Verknüpfung verweist, auf diesem Gerät synchronisiert ist und versuchen Sie es erneut.</translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="18"/>
        <source>Delete Account?</source>
        <translation>Account löschen?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="35"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation>Willst du wirklich das Konto &lt;stark&gt;%1&lt;/stark&gt; entfernen? Dadurch werden alle Bibliotheken, die zu diesem Konto gehören, von deinem Gerät entfernt.&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Hinweis: Du kannst diese vom Server wiederherstellen, indem du das Konto wieder hinzufügst.&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../qml/Actions/DeleteCompletedChildren.qml" line="10"/>
        <source>Delete Completed Items</source>
        <translation>Erledigte Einträge löschen</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="18"/>
        <source>Delete Completed Items?</source>
        <translation>Erledigte Einträge löschen?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Möchtest du wirklich alle erledigten Einträge in der Liste &lt;strong&gt;%1&lt;/strong&gt; löschen? Dies kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="43"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Möchtest du wirklich alle erledigten Aufgaben in der Liste &lt;strong&gt;%1&lt;/strong&gt; löschen? Dies kann nicht rückgängig gemacht werden.</translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../qml/Actions/DeleteItem.qml" line="10"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="18"/>
        <source>Delete Item?</source>
        <translation>Eintrag löschen?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="31"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Soll das Bild &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="35"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Soll die Aufgabenliste &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Soll die Aufgabe &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="43"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Soll die Unteraufgabe &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="46"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Soll die Notiz &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="49"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Möchtest du wirklich die Seite &lt;stark&gt;%1&lt;/stark&gt; löschen? Dies kann nicht rückgängig gemacht werden.</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="18"/>
        <source>Delete Library?</source>
        <translation>Bibliothek löschen?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="32"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation>Soll die Bibliothek &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? &lt;em&gt;Das wird alle Dateien, die zu ihr gehören, ebenfalls löschen.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="38"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation>Soll die Bibliothek &lt;strong&gt;%1&lt;/strong&gt; wirklich gelöscht werden? Dateien, die zur Bibliothek gehören, werden nicht gelöscht, so dass sie jederzeit wieder zur Anwendung hinzugefügt werden kann.</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="26"/>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="65"/>
        <source>Connection Settings</source>
        <translation>Verbindungs Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="73"/>
        <source>Trouble Signing In?</source>
        <translation>Probleme bei der Anmeldung?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="84"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation>Wir haben versucht, Ihren Browser zu öffnen, um Sie bei Ihrem Dropbox-Konto anzumelden. Bitte melden Sie sich an und gewähren Sie OpenTodoList Zugang, um fortzufahren.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="91"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation>Konnte Ihr Browser nicht geöffnet werden? Sie können es erneut versuchen, oder die gewünschte URL manuell in Ihren Browser kopieren.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="95"/>
        <source>Authorize...</source>
        <translation>Autorisieren...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="104"/>
        <source>Open Browser</source>
        <translation>Öffne den Browser</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="110"/>
        <source>Copy Link</source>
        <translation>Link kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="114"/>
        <source>Copied!</source>
        <translation>Kopiert!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="123"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="130"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="22"/>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="57"/>
        <source>Edit Account</source>
        <translation>Konto bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="63"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="74"/>
        <source>Server Address:</source>
        <translation>Server-Adresse:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="81"/>
        <source>https://myserver.example.com</source>
        <translation>https://meinserver.beispiel.de</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="89"/>
        <source>Login</source>
        <translation>Anmeldung</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="102"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="109"/>
        <source>User Name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="115"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="122"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="131"/>
        <source>Disable Certificate Checks</source>
        <translation>Zertifikatsüberprüfung deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="140"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Verbindung zum Server fehlgeschlagen. Bitte überprüfe Benutzername, Passwort und die Server-Adresse und versuche es erneut.</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="32"/>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="116"/>
        <source>Edit Account</source>
        <translation>Konto bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="122"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="133"/>
        <source>Server Address:</source>
        <translation>Serveradresse:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="139"/>
        <source>https://myserver.example.com</source>
        <translation>https://meinserver.beispiel.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="146"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="152"/>
        <source>User Name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="159"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="166"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="176"/>
        <source>Disable Certificate Checks</source>
        <translation>Zertifikatsprüfungen deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="184"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Verbindung zum Server fehlgeschlagen. Bitte überprüfe Deinen Benutzernamen, Dein Passwort, die Serveradresse und versuche es erneut.</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="68"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt; wurde erzeugt.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="74"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="79"/>
        <source>Dismiss</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="31"/>
        <source>Due on</source>
        <translation>Fällig am</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="69"/>
        <source>First due on %1.</source>
        <translation>Erstmals fällig am %1.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="88"/>
        <source>No recurrence pattern set...</source>
        <translation>Kein Rezidiv-Muster gesetzt...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="90"/>
        <source>Recurs every day.</source>
        <translation>Wiederholt sich jeden Tag.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="92"/>
        <source>Recurs every week.</source>
        <translation>Wiederholt sich jede Woche.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="94"/>
        <source>Recurs every month.</source>
        <translation>Wiederholt sich jeden Monat.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="98"/>
        <source>Recurs every %1 days.</source>
        <translation>Wiederholt sich alle %1 Tage.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="96"/>
        <source>Recurs every year.</source>
        <translation>Jährlich wiederkehrend.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="101"/>
        <source>Recurs every %1 weeks.</source>
        <translation>Wiederkehrend alle %1 Wochen.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="104"/>
        <source>Recurs every %1 months.</source>
        <translation>Wiederkehrend alle %1 Monate.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="141"/>
        <source>Recurs until %1.</source>
        <translation>Wiederholt sich bis 1%.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="144"/>
        <source>Recurs indefinitely</source>
        <translation>Unbegrenzt wiederkehrend</translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="28"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="73"/>
        <source>No notes added yet - click here to add some.</source>
        <translation>Noch keine Notizen vorhanden - klicke hier zum Hinzufügen.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="160"/>
        <source>Export to File...</source>
        <translation>In eine Datei exportieren...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>Markdown files</source>
        <translation>Markdown-Dateien</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="106"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="113"/>
        <source>Copy Formatted Text</source>
        <translation>Formatierten Text kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="129"/>
        <source>Copy Plain Text</source>
        <translation>Reinen Text kopieren</translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="190"/>
        <source>Move Todo Into...</source>
        <translation>Verschiebe Todo zu ...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="220"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation>Unteraufgabe in Aufgabe konvertieren und verschieben nach …</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="236"/>
        <source>Copy Item Into...</source>
        <translation>Artikel kopieren in...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="253"/>
        <source>Copy Todo Into...</source>
        <translation>Todo kopieren in ...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="205"/>
        <source>Move Task Into...</source>
        <translation>Aufgabe verschieben nach …</translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="55"/>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="223"/>
        <source>Schedule</source>
        <translation>Zeitplan</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="85"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="78"/>
        <source>Edit List</source>
        <translation>Liste bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Hide Schedule</source>
        <translation>Zeitplan ausblenden</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Show Schedule</source>
        <translation>Zeige Zeitplan</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="201"/>
        <source>Move Up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="210"/>
        <source>Move Down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="93"/>
        <source>Donate</source>
        <translation>Spenden</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="293"/>
        <source>Untagged</source>
        <translation>Ungetaggt</translation>
    </message>
</context>
<context>
    <name>Library</name>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="615"/>
        <source>Unable to create backup folder</source>
        <translation>Backupordner kann nicht angelegt werden</translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="619"/>
        <source>Failed to change into backup folder</source>
        <translation>Es kann nicht in den Backupordner gewechselt werden</translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="630"/>
        <source>Failed to open file for writing: %1</source>
        <translation>Die Datei kann nicht zum Schreiben geöffnet werden: %1</translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="210"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="217"/>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="224"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="231"/>
        <source>Yellow</source>
        <translation>Gelb</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="238"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="245"/>
        <source>Lilac</source>
        <translation>Lila</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="252"/>
        <source>White</source>
        <translation>Weiß</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="262"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="273"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="355"/>
        <source>Note Title</source>
        <translation>Titel der Notiz</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="368"/>
        <source>Todo List Title</source>
        <translation>Titel der Aufgabenliste</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="381"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Suchbegriff 1, Suchbegriff 2, …</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="458"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation>Noch nichts hier! Die Bibliothek kann mit &lt;a href=&apos;#note&apos;&gt;Notizen&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;Aufgabenlisten&lt;/a&gt; oder &lt;a href=&apos;#image&apos;&gt;Bildern&lt;/a&gt; befüllt werden.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="514"/>
        <source>Sort By</source>
        <translation>Sortierung</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="518"/>
        <source>Manually</source>
        <translation>Manuell</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="525"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="532"/>
        <source>Due To</source>
        <translation>Fällig am</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="539"/>
        <source>Created At</source>
        <translation>Angelegt am</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="546"/>
        <source>Updated At</source>
        <translation>Aktualisiert am</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="267"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="285"/>
        <source>Select Image</source>
        <translation>Bild auswählen</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="13"/>
        <source>Synchronization Log</source>
        <translation>Synchronisationsereignisse</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="68"/>
        <source>Debugging information</source>
        <translation>Informationen zur Fehlerbeseitigung</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="70"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="72"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="74"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="76"/>
        <source>Upload</source>
        <translation>Upload</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="78"/>
        <source>Create local folder</source>
        <translation>Anlegen eines lokalen Verzeichnisses</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="80"/>
        <source>Create remote folder</source>
        <translation>Anlegen eines Verzeichnisses auf dem Server</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="82"/>
        <source>Deleting locally</source>
        <translation>Lokal löschen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="84"/>
        <source>Deleting remotely</source>
        <translation>Auf dem Server löschen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="86"/>
        <source>Unknown log message type</source>
        <translation>Unbekannter Benachrichtigungstyp</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="23"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="197"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation>Eine &lt;a href=&apos;#newLibrary&apos;&gt;neue Bibliothek anlegen&lt;/a&gt;. Bibliotheken enthalten verschiedene Einträge, wie Notizen, Aufgabenlisten und Bilder.</translation>
    </message>
</context>
<context>
    <name>MarkFutureInstanceAsDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="13"/>
        <source>%1 is scheduled for the future - do you want to mark that future instance as done?</source>
        <translation>%1 ist in der Zukunft geplant - möchten Sie diese zukünftige Instanz als erledigt markieren?</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation>Als erledigt markieren</translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="29"/>
        <source>Keep Open</source>
        <translation>Geöffnet halten</translation>
    </message>
</context>
<context>
    <name>MoveTask</name>
    <message>
        <location filename="../qml/Actions/MoveTask.qml" line="12"/>
        <source>Move</source>
        <translation>Verschieben</translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../qml/Actions/MoveTodo.qml" line="12"/>
        <source>Move</source>
        <translation>Verschieben</translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="53"/>
        <source>Connection Settings</source>
        <translation>Verbindungseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="61"/>
        <source>Trouble Signing In?</source>
        <translation>Probleme bei der Anmeldung?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="72"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation>Wir haben versucht, Ihren Browser zu öffnen, um Sie bei Ihrem Dropbox-Konto anzumelden. Bitte melden Sie sich an und gewähren Sie OpenTodoList Zugang, um fortzufahren.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="79"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation>Konnte Ihr Browser nicht geöffnet werden? Sie können es erneut versuchen, oder die gewünschte URL manuell in Ihren Browser kopieren.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="85"/>
        <source>Authorize...</source>
        <translation>Autorisieren...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="94"/>
        <source>Open Browser</source>
        <translation>Browser öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="100"/>
        <source>Copy Link</source>
        <translation>Link kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="104"/>
        <source>Copied!</source>
        <translation>Kopiert!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="113"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="120"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="104"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="105"/>
        <source>Tomorrow</source>
        <translation>Morgen</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="106"/>
        <source>This Week</source>
        <translation>Diese Woche</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="107"/>
        <source>Next Week</source>
        <translation>Nächste Woche</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="108"/>
        <source>Select...</source>
        <translation>Auswählen...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="138"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="143"/>
        <source>The title for your new item...</source>
        <translation>Der Titel für Deinen neuen Artikel...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="185"/>
        <source>Create in:</source>
        <translation>Erstellt am:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="226"/>
        <source>Due on:</source>
        <translation>Fällig am:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="149"/>
        <source>Library</source>
        <translation>Bibliothek</translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="81"/>
        <source>Create Library in Account</source>
        <translation>Bibliothek im Konto anlegen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="90"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation>Eine in einem Konto erstellte Bibliothek wird mit diesem Konto synchronisiert. Dies ermöglicht es, eine Bibliothek einfach auf einem Server zu sichern und später von dort aus wiederherzustellen. Zusätzlich können solche Bibliotheken mit anderen Benutzern gemeinsam genutzt werden (wenn der Server dies erlaubt).</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="98"/>
        <source>Existing Libraries</source>
        <translation>Vorhandene Bibliotheken</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="106"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation>Wähle eine vorhandene Bibliothek auf dem Server aus, um sie zur App hinzuzufügen.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="112"/>
        <source>No libraries were found on the server.</source>
        <translation>Auf dem Server wurden keine Bibliotheken gefunden.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="121"/>
        <source>Searching existing libraries...</source>
        <translation>Bestehende Bibliotheken durchsuchen...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="164"/>
        <source>Create a New Library</source>
        <translation>Neue Bibliothek erstellen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="172"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation>Erstellen eine neue Bibliothek, die mit dem Server synchronisiert wird. Eine solche Bibliothek kann der App auch auf anderen Geräten hinzugefügt werden, um Daten zu synchronisieren.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="186"/>
        <source>My new library&apos;s name</source>
        <translation>Der Name meiner neuen Bibliothek</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="100"/>
        <source>Open a Folder as a Library</source>
        <translation>Öffne einen Ordner als Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="108"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation>Du kannst einen beliebigen Ordner als Speicher für eine Bibliothek nutzen.&lt;br/&gt;&lt;br/&gt;Das ist besonders nützlich, wenn du eine andere Anwendung (wie den Sync-Client eines Cloud-Anbieters) nutzen willst, um deine Daten mit dem Server zu synchronisieren.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="112"/>
        <source>Folder:</source>
        <translation>Ordner:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="117"/>
        <source>Path to a folder to use as a library</source>
        <translation>Pfad zu einem Ordner, der als Bibliothek genutzt werden soll</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="123"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="128"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="133"/>
        <source>My Local Library Name</source>
        <translation>Name meiner lokalen Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation>Einen Ordner auswählen</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="22"/>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="62"/>
        <source>Create Library</source>
        <translation>Bibliothek anlegen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="69"/>
        <source>Local Library</source>
        <translation>Lokale Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="76"/>
        <source>Use Folder as Library</source>
        <translation>Ordner als Bibliothek nutzen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="88"/>
        <source>Add Libraries From Your Accounts</source>
        <translation>Bibliotheken von deinen Konten hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="115"/>
        <source>Add Account</source>
        <translation>Konto hinzufügen</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="61"/>
        <source>Create a Local Library</source>
        <translation>Lokale Bibliothek erstellen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="69"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation>Eine lokale Bibliothek wird nur auf deinem Gerät gespeichert - sie eignet sich perfekt für die Wahrung deiner Privatsphäre!&lt;br/&gt;&lt;br/&gt;Nutze sie, wenn du deine Informationen nur lokal speichern und deine Daten regelmäßig über andere Mechanismen sichern willst. Wenn du dagegen über mehrere Geräte hinweg Zugang zu deinen Informationen möchtest, erstelle stattdessen eine synchronisierte Bibliothek.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="79"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="84"/>
        <source>My Local Library Name</source>
        <translation>Name meiner lokalen Bibliothek</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="19"/>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="71"/>
        <source>Connection Settings</source>
        <translation>Verbindungseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="77"/>
        <source>Server Address:</source>
        <translation>Server-Adresse:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="107"/>
        <source>Trouble Signing In?</source>
        <translation>Probleme beim Anmelden?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="118"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation>Wir haben versucht, den Browser zu öffnen, um dich bei deiner Nextcloud-Instanz anzumelden. Bitte melde dich an und erlaube OpenTodoList den Zugriff, um fortzufahren. Probleme beim Erreichen deiner NextCloud im Browser? Du kannst Benutzername und Passwort auch manuell eingeben.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="126"/>
        <source>Log in Manually</source>
        <translation>Manuell anmelden</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="132"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation>Idealerweise benutzt du App-spezifische Passwörter statt deines Benutzer-Passworts. Falls deine Nextcloud mit Zwei-Faktor-Authentifizierung (2FA) geschützt ist, ist ein App-spezifisches Passwort sogar verpflichtend. Du kannst ein solches Passwort in den Nextcloud-Einstellungen &gt; Sicherheit erzeugen.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="141"/>
        <source>Create App Password</source>
        <translation>Erzeuge App-Passwort</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="150"/>
        <source>Account Settings</source>
        <translation>Konto-Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="160"/>
        <source>Copy Link</source>
        <translation>Link kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="166"/>
        <source>Copied!</source>
        <translation>Kopiert!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="175"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="182"/>
        <source>User Name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="189"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="196"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="206"/>
        <source>Disable Certificate Checks</source>
        <translation>Zertifikatsüberprüfung deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="210"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="223"/>
        <source>Account Name</source>
        <translation>Kontoname</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="233"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Verbindung zum Server fehlgeschlagen. Bitte überprüfe Benutzername, Passwort und die Server-Adresse und versuche es erneut.</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="52"/>
        <source>Note</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="41"/>
        <source>Todo List</source>
        <translation>Aufgabenliste</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="56"/>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="46"/>
        <source>Todo</source>
        <translation>Todo</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="161"/>
        <source>Account Name</source>
        <translation>Kontoname</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="28"/>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="99"/>
        <source>Connection Settings</source>
        <translation>Verbindungseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="105"/>
        <source>Server Address:</source>
        <translation>Serveradresse:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="111"/>
        <source>https://myserver.example.com</source>
        <translation>https://meinserver.beispiel.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="117"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="123"/>
        <source>User Name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="129"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="135"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="144"/>
        <source>Disable Certificate Checks</source>
        <translation>Zertifikatsprüfungen deaktivieren</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="148"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="171"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Verbindung zum Server fehlgeschlagen. Bitte überprüfe Deinen Benutzernamen, Dein Passwort, die Serveradresse und versuche es erneut.</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../qml/Widgets/NoteItem.qml" line="91"/>
        <source>Due on %1</source>
        <translation>Fällig am %1</translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="171"/>
        <source>Main Page</source>
        <translation>Startseite</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="238"/>
        <source>New Page</source>
        <translation>Neue Seite</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation>System Sprache</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation>Fehlende Zugangsdaten für das Konto</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="18"/>
        <location filename="../qml/Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation>Erkannte Probleme</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation>Synchronisation der Bibliothek ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="91"/>
        <source>Retry Sync</source>
        <translation>Synchronisation erneut versuchen</translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../qml/Actions/PromoteTask.qml" line="13"/>
        <source>Promote</source>
        <translation>In Aufgabe konvertieren</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="312"/>
        <location filename="../appstartup.cpp" line="319"/>
        <source>unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="17"/>
        <source>Quick Notes</source>
        <translation>Kurznotiz</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="36"/>
        <source>Quick Notes Editor</source>
        <translation>Kurznotiz Editor</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="45"/>
        <source>Open the main window</source>
        <translation>Hauptfenster öffnen</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="57"/>
        <source>Quick Note Title</source>
        <translation>Kurznotiz-Titel</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="121"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="126"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation>Speichere die Notizen in die ausgewählte Bibliothek. Halte den Button gedrückt für mehr Speicheroptionen.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="148"/>
        <source>Save as Note</source>
        <translation>Als Notiz speichern</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="161"/>
        <source>Save as Todo List</source>
        <translation>Als Todo-Liste speichern</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="165"/>
        <source>Quick Todo List</source>
        <translation>Kurz-Todo-Liste</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="152"/>
        <source>Quick Note</source>
        <translation>Kurznotiz</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="70"/>
        <source>Type your notes here...</source>
        <translation>Schreibe hier deine Notizen...</translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="29"/>
        <source>Edit Recurrence</source>
        <translation>Wiederholung bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="47"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="51"/>
        <source>Weekly</source>
        <translation>Wöchentlich</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="55"/>
        <source>Monthly</source>
        <translation>Monatlich</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="63"/>
        <source>Every N Days</source>
        <translation>Alle N Tage</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="97"/>
        <source>Recurs:</source>
        <translation>Wiederholt sich:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="115"/>
        <source>Number of days:</source>
        <translation>Anzahl von Tagen:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="141"/>
        <source>Recur relative to the date when marking as done</source>
        <translation>Wiederholung relativ zum Datum, wenn als erledigt markiert wird</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="59"/>
        <source>Yearly</source>
        <translation>Jährlich</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="67"/>
        <source>Every N Weeks</source>
        <translation>Alle N Wochen</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="71"/>
        <source>Every N Months</source>
        <translation>Alle N Monate</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="117"/>
        <source>Number of weeks:</source>
        <translation>Wochenzahl:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="119"/>
        <source>Number of months:</source>
        <translation>Anzahl der Monate:</translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../qml/Actions/RenameItem.qml" line="10"/>
        <source>Rename</source>
        <translation>Umbenennung</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="21"/>
        <source>Rename Item</source>
        <translation>Eintrag umbenennen</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="32"/>
        <source>Enter item title...</source>
        <translation>Titel für den Eintrag ...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="19"/>
        <source>Rename Library</source>
        <translation>Bibliothek umbenennen</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="36"/>
        <source>Enter library title...</source>
        <translation>Bibliotheksnamen eingeben...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../qml/Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation>Zurücksetzen aufgrund von</translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="147"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="148"/>
        <source>Tomorrow</source>
        <translation>Morgen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="184"/>
        <source>Later This Week</source>
        <translation>Später in dieser Woche</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="186"/>
        <source>Next Week</source>
        <translation>Nächste Woche</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="187"/>
        <source>Coming Next</source>
        <translation>als nächstes geplant</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="266"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation>Nichts geplant... Einträge, die ein Enddatum gesetzt haben, werden in dieser Liste angezeigt.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="123"/>
        <source>Overdue</source>
        <translation>Überfällig</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="52"/>
        <source>Schedule</source>
        <translation>Zeitplan</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/SelectLibraryDialog.qml" line="21"/>
        <source>Select Library</source>
        <translation>Bibliothek wählen</translation>
    </message>
</context>
<context>
    <name>SelectTodoDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="22"/>
        <source>Select Todo</source>
        <translation>Aufgabe auswählen</translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="42"/>
        <source>Todo List:</source>
        <translation>Aufgabenliste:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="64"/>
        <source>Todo:</source>
        <translation>Aufgabe:</translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoListDialog.qml" line="24"/>
        <source>Select Todo List</source>
        <translation>Wähle Todo-Liste</translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTopLevelItemDialog.qml" line="21"/>
        <source>Select Item</source>
        <translation>Eintrag auswählen</translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation>Diese Woche fällig</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation>Nächste Woche fällig</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../qml/Actions/SetDueTo.qml" line="10"/>
        <source>Select Due Date</source>
        <translation>Fälligkeitsdatum auswählen</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../qml/Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation>Heute fällig</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../qml/Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation>Morgen fällig</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../qml/Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation>Fortschritt setzen</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="46"/>
        <source>User Interface</source>
        <translation>Benutzeroberfläche</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="54"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="118"/>
        <source>Theme:</source>
        <translation>Farbschema:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="208"/>
        <source>Font Size:</source>
        <translation>Schriftgröße:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="225"/>
        <source>Use custom font size</source>
        <translation>Eigene Schriftgröße nutzen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="243"/>
        <source>Use Compact Style</source>
        <translation>Kompakten Stil nutzen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="252"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation>Abstände zwischen den Komponenten verringern und Schriftgröße reduzieren.

&lt;em&gt;Benötigt einen Neustart der Anwendung.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="260"/>
        <source>Use compact todo lists</source>
        <translation>Kompakte Todo-Listen nutzen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="270"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation>Vertikale Abstände in Todo- und Aufgabenlisten reduzieren, um Platz für mehr Elemente auf dem Bildschirm zu schaffen.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="277"/>
        <source>Override Scaling Factor</source>
        <translation>Skalierungsfaktor überschreiben</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="284"/>
        <source>Scale Factor:</source>
        <translation>Skalierungsfaktor:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="304"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation>Nutze dies, um die Benutzeroberfläche manuell zu skalieren. Standardmäßig sollte sich die Anwendung entsprechend der Geräteeinstellungen automatisch anpassen. Wenn das nicht funktioniert, kannst du hier einen eigenen Skalierungsfaktor einstellen.

Dies erfordert einen Neustart der Anwendung.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="313"/>
        <source>Library Item Size:</source>
        <translation>Kachelgröße in Bibliotheksansicht:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="189"/>
        <source>System Tray:</source>
        <translation>Systemablage:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="202"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation>Öffne Kurznotiz-Editor bei Klick</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="234"/>
        <source>Desktop Mode</source>
        <translation>Desktopmodus</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="329"/>
        <source>Show notes excerpt in listings</source>
        <translation>Auszug aus den Notizen in den Inseraten anzeigen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="194"/>
        <source>Monochrome Icon</source>
        <translation>Einfarbiges Icon</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="143"/>
        <source>Custom Primary Color:</source>
        <translation>Eigene Primärfarbe:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Pages/SettingsPage.qml" line="171"/>
        <source>Select</source>
        <translation>Auswählen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="165"/>
        <source>Custom Secondary Color:</source>
        <translation>Eigene Sekundärfarbe:</translation>
    </message>
</context>
<context>
    <name>StackViewWindow</name>
    <message>
        <location filename="../qml/Windows/StackViewWindow.qml" line="21"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="45"/>
        <source>Libraries</source>
        <translation>Bibliotheken</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="70"/>
        <source>Add a new library</source>
        <translation>Neue Bibliothek hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="89"/>
        <source>Accounts</source>
        <translation>Konten</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="111"/>
        <source>Add an account</source>
        <translation>Konto hinzufügen</translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="42"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation>Es sind Fehler beim Synchronisieren der Anwendung aufgetreten. Bitte sicherstellen, dass die Einstellungen der Bibliothek korrekt sind.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="48"/>
        <source>Ignore</source>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="52"/>
        <source>View</source>
        <translation>Überprüfen</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../qml/Widgets/TagsEditor.qml" line="32"/>
        <source>Add Tag</source>
        <translation>Neues Schlagwort hinzufügen</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="136"/>
        <source>✔ No open todos - everything done</source>
        <translation>✔ Keine offenen Aufgaben - alles erledigt</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="77"/>
        <source>Due on %1</source>
        <translation>Fällig am %1</translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="166"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Suchbegriff 1, Suchbegriff 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="253"/>
        <source>Todos</source>
        <translation>Aufgaben</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="273"/>
        <source>Add new todo...</source>
        <translation>Neue Aufgabe hinzufügen...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="178"/>
        <source>Manually</source>
        <translation>Manuell</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="184"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="190"/>
        <source>Due Date</source>
        <translation>Fällig am</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="196"/>
        <source>Created At</source>
        <translation>Angelegt am</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="202"/>
        <source>Updated At</source>
        <translation>Aktualisiert am</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="215"/>
        <source>Show Completed</source>
        <translation>Abgeschlossene anzeigen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="223"/>
        <source>Show At The End</source>
        <translation>Am Ende anzeigen</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="181"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Suchbegriff 1, Suchbegriff 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="206"/>
        <source>Tasks</source>
        <translation>Unteraufgaben</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="212"/>
        <source>Add new task...</source>
        <translation>Neue Unteraufgabe hinzufügen...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="303"/>
        <source>Show Completed</source>
        <translation>Abgeschlossene anzeigen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="310"/>
        <source>Show At The End</source>
        <translation>Am Ende anzeigen</translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../qml/Widgets/TodosWidget.qml" line="211"/>
        <source>Due on: %1</source>
        <translation>Fällig am: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../qml/Widgets/TodosWidgetDelegate.qml" line="238"/>
        <source>More Actions...</source>
        <translation>Weitere Aktionen...</translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation>Eine neue Version von OpenTodoList %1 ist verfügbar.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
</context>
</TS>
