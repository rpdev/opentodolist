import QtQuick 2.0

import OpenTodoList.Style as C

C.Action {
    property string symbol: ""
    property C.Menu menu: null
    property bool visible: true
}
