import QtQuick
import QtQuick.Controls.Basic as Basic

Basic.ToolBar {
    palette.text: ColorTheme.textColorForBackgroundColor(palette.button)
}
