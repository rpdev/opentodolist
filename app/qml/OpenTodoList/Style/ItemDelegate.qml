import QtQuick
import QtQuick.Controls.Basic as Basic

Basic.ItemDelegate {
    id: control

    padding: StyleSettings.useDesktopMode ? 12 : 16
}
