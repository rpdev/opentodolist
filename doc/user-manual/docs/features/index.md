# Features

A lot of work went into developing OpenTodoList. Consequentially, the app is
quite right of features. In this chapter, we try to document all of them. To
approach this in a meaningful way, the chapter is subdivided into the following
parts:

## Backup

Better safe than sorry! Sometimes it is good to keep an extra copy of your data.
OpenTodoList has some mechanisms build into it to aid in this common task. These
are documented in the [Backup](./backup/index.md) section.

## Sync

Nowadays, it is rather common to have several devices and be able to take
your data with you. OpenTodoList supports this approach as well - all the
necessary information needed to set up libraries that follow you from
device to device are documented in the [Sync](./sync/index.md) section.