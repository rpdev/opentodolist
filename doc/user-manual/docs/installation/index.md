# Installation

There are two ways to install OpenTodoList: You can install it from a pre-built
package or build it yourself from source. The former should be preferred - it
is faster and you get regular updates. Right now, OpenTodoList comes with
installation options for the following systems and platforms:

- [Android](./android.md)
- [iOS](./ios.md)
- [Linux](./linux.md)
- [macOS](./macos.md)
- [Windows](./windows.md)

If you need the app on another system, you still can give it a try. As
OpenTodoList is available as Open Source, you can build it on your own -
the only prerequisite is that the [Qt](https://qt.io) framework is available
for your target system. If you want to try that path, continue reading in the
[Building from Source](./source.md) section.