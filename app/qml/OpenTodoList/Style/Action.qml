import QtQuick
import QtQuick.Controls.Basic as Basic

Basic.Action {


    /**
     * @brief The symbol to represent the action when a symbol font is used.
     */
    property string symbol: ""
}
