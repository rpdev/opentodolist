# Basics

OpenTodoList has grown for several years now, undergoing smaller and larger
changes over that time. During that, certain base properties and philosophies
have been incorporated into the app. In this section, we want to explain these
a bit, giving you - as a user - the chance to better understand why things
work in a certain way in the app.

## Data Model

The [Data Model](./data_model/index.md) part describes the different types of
entities used within the app. This should give you a good understanding about
what is meant when certain terms are used within the app. After reading this,
you should be able to understand which basic capabilities the app provides and
which type of item can be used for which purposes.
