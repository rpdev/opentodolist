<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tk">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation>About...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="33"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="40"/>
        <source>A todo and task managing application.</source>
        <translation>Todo we meseläni dolandyrmak programmasy.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="68"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation>OpenTodoList &lt;a href=&apos;app-license&apos;&gt; GNU General Public License &lt;/a&gt; 3-nji wersiýasy ýa-da soňraky wersiýasy boýunça goýberilýär.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="85"/>
        <source>Report an Issue</source>
        <translation>Bir mesele barada habar beriň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="114"/>
        <source>Third Party Libraries and Resources</source>
        <translation>Üçünji tarapyň kitaphanalary we çeşmeleri</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="151"/>
        <source>Author:</source>
        <translation>Awtor:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="156"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="163"/>
        <source>License:</source>
        <translation>Ygtyýarnama:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="168"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="174"/>
        <source>Download:</source>
        <translation>Göçürip al:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="178"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="92"/>
        <source>Copy System Information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="20"/>
        <source>Select Account Type</source>
        <translation type="unfinished">Hasap görnüşini saýlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="55"/>
        <source>Account Type</source>
        <translation type="unfinished">Hasap görnüşi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="61"/>
        <source>NextCloud</source>
        <translation type="unfinished">NextCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="64"/>
        <source>ownCloud</source>
        <translation type="unfinished">ownCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="67"/>
        <source>WebDAV</source>
        <translation type="unfinished">WebDAV</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="70"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../qml/Pages/AccountsPage.qml" line="18"/>
        <location filename="../qml/Pages/AccountsPage.qml" line="40"/>
        <source>Accounts</source>
        <translation type="unfinished">Hasaplar</translation>
    </message>
</context>
<context>
    <name>AllSubtasksDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="13"/>
        <source>Everything in %1 done! Do you want to mark it as well as done?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="30"/>
        <source>Keep Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="200"/>
        <source>Manage your personal data.</source>
        <translation type="unfinished">Şahsy maglumatlaryňyzy dolandyryň.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="206"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation type="unfinished">Duýgur ekranlar üçin käbir optimizasiýalary açyň.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="209"/>
        <source>Only run the app background service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="211"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="216"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="367"/>
        <source>Open</source>
        <translation type="unfinished">Aç</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="433"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="375"/>
        <source>Quick Note</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="219"/>
        <source>Background Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="223"/>
        <source>App continues to sync your data in the background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="227"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationMenu</name>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="36"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="87"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="158"/>
        <source>Navigate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="197"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="18"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="27"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="35"/>
        <source>Accounts</source>
        <translation type="unfinished">Hasaplar</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="43"/>
        <source>Add Tag</source>
        <translation type="unfinished">Bellik goşuň</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="51"/>
        <source>Attach File</source>
        <translation type="unfinished">Faýl dakyň</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="59"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="66"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="73"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="81"/>
        <source>Copy Link To Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="89"/>
        <source>Create Sample Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="159"/>
        <source>Delete</source>
        <translation type="unfinished">Öçür</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="167"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="175"/>
        <source>Due Date</source>
        <translation type="unfinished">Berlen senesi</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="183"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="190"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="207"/>
        <source>Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="213"/>
        <source>Mark all items as done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="225"/>
        <source>Mark all items as undone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="237"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="245"/>
        <source>New Library</source>
        <translation type="unfinished">Täze kitaphana</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="253"/>
        <source>Open Created Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="260"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="268"/>
        <source>Open Library Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="274"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="280"/>
        <source>Rename</source>
        <translation type="unfinished">Adyny üýtgetmek</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="288"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="309"/>
        <source>Scroll to Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="316"/>
        <source>Scroll to Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="323"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="330"/>
        <source>Set Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="337"/>
        <source>Sort</source>
        <translation type="unfinished">Sort</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="345"/>
        <source>Sync Now</source>
        <translation type="unfinished">Indi sinhronlaň</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="358"/>
        <source>Sync Log</source>
        <translation type="unfinished">Sinhron ýazgysy</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="369"/>
        <source>Translate The App...</source>
        <translation type="unfinished">Programmany terjime et ...</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../qml/Components/ApplicationToolBar.qml" line="86"/>
        <source>Problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="36"/>
        <source>Attach File</source>
        <translation>Faýl dakyň</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="51"/>
        <source>Delete Attachment?</source>
        <translation>Goşundyny pozuň?</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="55"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation>Goşundyny &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýändigiňize ynanýarsyňyzmy? Bu hereketi yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="69"/>
        <source>Attachments</source>
        <translation>Goşundylar</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation>Reňk</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation>Ak</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation>Gyzyl</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation>Greenaşyl</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation>Gök</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation>Sary</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation>Mämişi</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation>Kirpik</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation>Ulgam</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation>Lightagtylyk</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation>Garaňky</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../qml/Actions/CopyTodo.qml" line="12"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../qml/Actions/CopyTopLevelItem.qml" line="12"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeepLinkHandler</name>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="56"/>
        <source>Uuups... seems that&apos;s a dead end...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="65"/>
        <source>Sorry, that link does not lead to any page or item that is present on this device. Check if the library or item to which the link points is synchronized on this device and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="18"/>
        <source>Delete Account?</source>
        <translation>Hasaby pozuň?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="35"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation>Hakykatdanam &lt;strong&gt;%1&lt;/strong&gt; hasaby aýyrmak isleýärsiňizmi? Bu hasabyňyza degişli ähli kitaphanalary enjamyňyzdan aýyrar? &lt;br/&gt; &lt;br/&gt; &lt;i&gt; Bellik: Hasaby yzyna goşup serwerden dikeldip bilersiňiz. &lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../qml/Actions/DeleteCompletedChildren.qml" line="10"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="18"/>
        <source>Delete Completed Items?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="43"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../qml/Actions/DeleteItem.qml" line="10"/>
        <source>Delete</source>
        <translation>Öçür</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="18"/>
        <source>Delete Item?</source>
        <translation>Öçürmek?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="31"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Goşundyny &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýändigiňize ynanýarsyňyzmy? Bu hereketi yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="35"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Todo sanawyny &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Hakykatdanam todo &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="43"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Hakykatdanam &lt;strong&gt;%1&lt;/strong&gt; meseläni pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="46"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Hakykatdanam &lt;strong&gt;%1&lt;/strong&gt; belligi pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="49"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Sahypany hakykatdanam pozmak isleýärsiňizmi? &lt;strong&gt;%1&lt;/strong&gt;? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="18"/>
        <source>Delete Library?</source>
        <translation>Kitaphanany pozuň?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="32"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation>Hitaphanany &lt;strong&gt;%1&lt;/strong&gt; programmadan aýyrmak isleýärsiňizmi? &lt;em&gt; Bu kitaphana degişli islendik faýly aýyrar. &lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="38"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation>Kitaphanany &lt;strong&gt;%1&lt;/strong&gt; programmadan aýyrmak isleýärsiňizmi? Kitaphananyň içindäki faýllaryň aýrylmajakdygyna üns beriň, soň bolsa kitaphanany dikeldip bilersiňiz.</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="26"/>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="65"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="73"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="84"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="91"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="95"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="104"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="110"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="114"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="123"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="130"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="22"/>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="57"/>
        <source>Edit Account</source>
        <translation type="unfinished">Hasaby redaktirläň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="63"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="74"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="81"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="89"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="102"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="109"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="115"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="122"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="131"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="140"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="32"/>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="116"/>
        <source>Edit Account</source>
        <translation type="unfinished">Hasaby redaktirläň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="122"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="133"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="139"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="146"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="152"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="159"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="166"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="176"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="184"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="68"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt; döredildi.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="74"/>
        <source>Open</source>
        <translation>Aç</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="79"/>
        <source>Dismiss</source>
        <translation>Işden aýyrmak</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="31"/>
        <source>Due on</source>
        <translation>Berilmeli</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="69"/>
        <source>First due on %1.</source>
        <translation>Ilki bilen %1 tölemeli.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="88"/>
        <source>No recurrence pattern set...</source>
        <translation>Gaýtalanma nusgasy ýok ...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="90"/>
        <source>Recurs every day.</source>
        <translation>Her gün gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="92"/>
        <source>Recurs every week.</source>
        <translation>Her hepde gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="94"/>
        <source>Recurs every month.</source>
        <translation>Her aý gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="98"/>
        <source>Recurs every %1 days.</source>
        <translation>Her %1 günden gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="96"/>
        <source>Recurs every year.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="101"/>
        <source>Recurs every %1 weeks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="104"/>
        <source>Recurs every %1 months.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="141"/>
        <source>Recurs until %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="144"/>
        <source>Recurs indefinitely</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="28"/>
        <source>Notes</source>
        <translation>Bellikler</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="73"/>
        <source>No notes added yet - click here to add some.</source>
        <translation>Entek bellikler goşulmady - käbirlerini goşmak üçin şu ýere basyň.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="160"/>
        <source>Export to File...</source>
        <translation>Faýla eksport ...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>Markdown files</source>
        <translation>Markdown faýllary</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>All files</source>
        <translation>Fileshli faýllar</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="106"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="113"/>
        <source>Copy Formatted Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="129"/>
        <source>Copy Plain Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="190"/>
        <source>Move Todo Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="220"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="236"/>
        <source>Copy Item Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="253"/>
        <source>Copy Todo Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="205"/>
        <source>Move Task Into...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="55"/>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="223"/>
        <source>Schedule</source>
        <translation>Iş tertibi</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="85"/>
        <source>Settings</source>
        <translation>Sazlamalar</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="78"/>
        <source>Edit List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Hide Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Show Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="201"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="210"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="93"/>
        <source>Donate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="293"/>
        <source>Untagged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Library</name>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="615"/>
        <source>Unable to create backup folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="619"/>
        <source>Failed to change into backup folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="630"/>
        <source>Failed to open file for writing: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="210"/>
        <source>Red</source>
        <translation>Gyzyl</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="217"/>
        <source>Green</source>
        <translation>Greenaşyl</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="224"/>
        <source>Blue</source>
        <translation>Gök</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="231"/>
        <source>Yellow</source>
        <translation>Sary</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="238"/>
        <source>Orange</source>
        <translation>Mämişi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="245"/>
        <source>Lilac</source>
        <translation>Kirpik</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="252"/>
        <source>White</source>
        <translation>Ak</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="262"/>
        <source>Rename</source>
        <translation>Adyny üýtgetmek</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="273"/>
        <source>Delete</source>
        <translation>Öçür</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="355"/>
        <source>Note Title</source>
        <translation>Bellik ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="368"/>
        <source>Todo List Title</source>
        <translation>Todo sanawyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="381"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Search term 1, search term 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="458"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation>Bu ýerde entek hiç zat ýok! &lt;a href=&apos;#note&apos;&gt; bellik &lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt; todo sanawy &lt;/a&gt; ýa-da &lt;a href=&apos;#image&apos;&gt; surat &lt;/a&gt; goşmak bilen başlaň.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="514"/>
        <source>Sort By</source>
        <translation>Sort boýunça</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="518"/>
        <source>Manually</source>
        <translation>El bilen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="525"/>
        <source>Title</source>
        <translation>Ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="532"/>
        <source>Due To</source>
        <translation>Sebäbi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="539"/>
        <source>Created At</source>
        <translation>Döredildi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="546"/>
        <source>Updated At</source>
        <translation>Täzelendi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="267"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="285"/>
        <source>Select Image</source>
        <translation type="unfinished">Surat saýlaň</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="13"/>
        <source>Synchronization Log</source>
        <translation>Sinhronizasiýa gündeligi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="68"/>
        <source>Debugging information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="70"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="72"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="74"/>
        <source>Download</source>
        <translation type="unfinished">Göçürip al</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="76"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="78"/>
        <source>Create local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="80"/>
        <source>Create remote folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="82"/>
        <source>Deleting locally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="84"/>
        <source>Deleting remotely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="86"/>
        <source>Unknown log message type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="23"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="197"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation>&lt;a href=&apos;#newLibrary&apos;&gt; täze kitaphana döretmekden başlaň &lt;/a&gt;. Kitaphanalar bellikler, sanawlar we suratlar ýaly dürli zatlary saklamak üçin ulanylýar.</translation>
    </message>
</context>
<context>
    <name>MarkFutureInstanceAsDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="13"/>
        <source>%1 is scheduled for the future - do you want to mark that future instance as done?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="29"/>
        <source>Keep Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveTask</name>
    <message>
        <location filename="../qml/Actions/MoveTask.qml" line="12"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../qml/Actions/MoveTodo.qml" line="12"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="53"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="61"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="72"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="79"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="85"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="94"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="100"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="104"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="113"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="120"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="104"/>
        <source>Today</source>
        <translation>Bu gün</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="105"/>
        <source>Tomorrow</source>
        <translation>Ertir</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="106"/>
        <source>This Week</source>
        <translation>Bu hepde</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="107"/>
        <source>Next Week</source>
        <translation>Indiki hepde</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="108"/>
        <source>Select...</source>
        <translation>Saýlaň ...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="138"/>
        <source>Title:</source>
        <translation>Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="143"/>
        <source>The title for your new item...</source>
        <translation>Täze elementiňiziň ady ...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="185"/>
        <source>Create in:</source>
        <translation>Dörediň:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="226"/>
        <source>Due on:</source>
        <translation>Berilmeli:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="149"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="81"/>
        <source>Create Library in Account</source>
        <translation type="unfinished">Hasapda kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="90"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation type="unfinished">Hasapda döredilen kitaphana onuň bilen sinhronlaşdyrylýar. Bu kitaphanany serwere aňsatlyk bilen ätiýaçlamaga we soňra şol ýerden dikeltmäge mümkinçilik berýär. Mundan başga-da, şeýle kitaphanalary beýleki ulanyjylar bilen paýlaşyp bolýar (serwer muňa rugsat berse).</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="98"/>
        <source>Existing Libraries</source>
        <translation type="unfinished">Bar bolan kitaphanalar</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="106"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation type="unfinished">Programma goşmak üçin serwerde bar bolan kitaphanany saýlaň.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="112"/>
        <source>No libraries were found on the server.</source>
        <translation type="unfinished">Serwerde kitaphanalar tapylmady.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="121"/>
        <source>Searching existing libraries...</source>
        <translation type="unfinished">Bar bolan kitaphanalary gözlemek ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="164"/>
        <source>Create a New Library</source>
        <translation type="unfinished">Täze kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="172"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation type="unfinished">Serwer bilen sinhronlanjak täze kitaphana dörediň. Şeýle kitaphanany beýleki enjamlardaky programma goşmak bilen maglumatlary sinhronlamak üçin hem goşup bolýar.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="186"/>
        <source>My new library&apos;s name</source>
        <translation type="unfinished">Täze kitaphanamyň ady</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="100"/>
        <source>Open a Folder as a Library</source>
        <translation type="unfinished">Kitaphana hökmünde bukjany açyň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="108"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation type="unfinished">Islendik bukjany kitaphananyň ýerleşýän ýeri hökmünde ulanyp bilersiňiz. &lt;br/&gt; &lt;br/&gt; Maglumatlaryňyzy serwer bilen sinhronlamak üçin başga bir gural (bulut üpjünçisiniň sinhron müşderisi ýaly) ulanmak isleseňiz has peýdalydyr.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="112"/>
        <source>Folder:</source>
        <translation type="unfinished">Papka:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="117"/>
        <source>Path to a folder to use as a library</source>
        <translation type="unfinished">Kitaphana hökmünde ulanmak üçin bukja barýan ýol</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="123"/>
        <source>Select</source>
        <translation type="unfinished">Saýlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="128"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="133"/>
        <source>My Local Library Name</source>
        <translation type="unfinished">Localerli kitaphanamyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation type="unfinished">Papka saýlaň</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="22"/>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="62"/>
        <source>Create Library</source>
        <translation>Kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="69"/>
        <source>Local Library</source>
        <translation type="unfinished">Libraryerli kitaphana</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="76"/>
        <source>Use Folder as Library</source>
        <translation type="unfinished">Papkany kitaphana hökmünde ulanyň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="88"/>
        <source>Add Libraries From Your Accounts</source>
        <translation type="unfinished">Hasabyňyzdan kitaphanalary goşuň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="115"/>
        <source>Add Account</source>
        <translation type="unfinished">Hasap goşuň</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="61"/>
        <source>Create a Local Library</source>
        <translation type="unfinished">Libraryerli kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="69"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation type="unfinished">Libraryerli kitaphana diňe enjamyňyzda saklanýar - bu gizlinlik gysga bolmagy üçin ajaýyp edýär! &lt;br/&gt; &lt;br/&gt; Maglumatlary diňe ýerli derejede saklamak we ähli maglumatlaryňyzy beýleki mehanizmler arkaly yzygiderli ätiýaçlandyrmak isleseňiz ulanyň. Maglumatlaryňyza birnäçe enjamda girmek zerur bolsa, ýerine sinhronlanan kitaphanany dörediň.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="79"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="84"/>
        <source>My Local Library Name</source>
        <translation type="unfinished">Localerli kitaphanamyň ady</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="19"/>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="71"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="77"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="107"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="118"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="126"/>
        <source>Log in Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="132"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="141"/>
        <source>Create App Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="150"/>
        <source>Account Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="160"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="166"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="175"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="182"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="189"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="196"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="206"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="210"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="223"/>
        <source>Account Name</source>
        <translation type="unfinished">Hasabyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="233"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="52"/>
        <source>Note</source>
        <translation>Bellik</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="41"/>
        <source>Todo List</source>
        <translation>Todo sanawy</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="56"/>
        <source>Image</source>
        <translation>Surat</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="46"/>
        <source>Todo</source>
        <translation>Todo</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="161"/>
        <source>Account Name</source>
        <translation>Hasabyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="28"/>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="99"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="105"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="111"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="117"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="123"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="129"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="135"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="144"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="148"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="171"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../qml/Widgets/NoteItem.qml" line="91"/>
        <source>Due on %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="171"/>
        <source>Main Page</source>
        <translation>Baş sahypa</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="238"/>
        <source>New Page</source>
        <translation>Täze sahypa</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation type="unfinished">Ulgam dili</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation>Hasap üçin giriş maglumatlary ýok</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="18"/>
        <location filename="../qml/Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation type="unfinished">Meseleler tapyldy</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="91"/>
        <source>Retry Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../qml/Actions/PromoteTask.qml" line="13"/>
        <source>Promote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="312"/>
        <location filename="../appstartup.cpp" line="319"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="17"/>
        <source>Quick Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="36"/>
        <source>Quick Notes Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="45"/>
        <source>Open the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="57"/>
        <source>Quick Note Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="121"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="126"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="148"/>
        <source>Save as Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="161"/>
        <source>Save as Todo List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="165"/>
        <source>Quick Todo List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="152"/>
        <source>Quick Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="70"/>
        <source>Type your notes here...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="29"/>
        <source>Edit Recurrence</source>
        <translation>Urrygy-ýygydan redaktirläň</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation>Hiç haçan</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="47"/>
        <source>Daily</source>
        <translation>Her gün</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="51"/>
        <source>Weekly</source>
        <translation>Hepdelik</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="55"/>
        <source>Monthly</source>
        <translation>Aýlyk</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="63"/>
        <source>Every N Days</source>
        <translation>Her N gün</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="97"/>
        <source>Recurs:</source>
        <translation>Gaýtalama:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="115"/>
        <source>Number of days:</source>
        <translation>Günleriň sany:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="141"/>
        <source>Recur relative to the date when marking as done</source>
        <translation>Edilişi ýaly bellik edilende senä görä gaýtalaň</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="59"/>
        <source>Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="67"/>
        <source>Every N Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="71"/>
        <source>Every N Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="117"/>
        <source>Number of weeks:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="119"/>
        <source>Number of months:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../qml/Actions/RenameItem.qml" line="10"/>
        <source>Rename</source>
        <translation>Adyny üýtgetmek</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="21"/>
        <source>Rename Item</source>
        <translation>Harydyň adyny üýtgediň</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="32"/>
        <source>Enter item title...</source>
        <translation>Harydyň adyny giriziň ...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="19"/>
        <source>Rename Library</source>
        <translation>Kitaphananyň adyny üýtgediň</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="36"/>
        <source>Enter library title...</source>
        <translation>Kitaphananyň adyny giriziň ...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../qml/Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="147"/>
        <source>Today</source>
        <translation>Bu gün</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="148"/>
        <source>Tomorrow</source>
        <translation>Ertir</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="184"/>
        <source>Later This Week</source>
        <translation>Bu hepde soň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="186"/>
        <source>Next Week</source>
        <translation>Indiki hepde</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="187"/>
        <source>Coming Next</source>
        <translation>Indiki</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="266"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation>Hiç zat meýilleşdirilmedi ... Bu ýerde peýda bolmagy üçin elementlere bellenilen senäni goşuň.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="123"/>
        <source>Overdue</source>
        <translation>Wagtyň geçmegi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="52"/>
        <source>Schedule</source>
        <translation type="unfinished">Iş tertibi</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/SelectLibraryDialog.qml" line="21"/>
        <source>Select Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTodoDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="22"/>
        <source>Select Todo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="42"/>
        <source>Todo List:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="64"/>
        <source>Todo:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoListDialog.qml" line="24"/>
        <source>Select Todo List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTopLevelItemDialog.qml" line="21"/>
        <source>Select Item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation>Şu hepde bellemeli</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation>Geljek hepde bellemeli</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../qml/Actions/SetDueTo.qml" line="10"/>
        <source>Select Due Date</source>
        <translation>Berlen senäni saýlaň</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../qml/Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation>Şu gün bellemeli</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../qml/Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation>Ertir bermeli</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../qml/Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation type="unfinished">Sazlamalar</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="46"/>
        <source>User Interface</source>
        <translation type="unfinished">Ulanyjy interfeýsi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="54"/>
        <source>Language:</source>
        <translation type="unfinished">Dil:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="118"/>
        <source>Theme:</source>
        <translation type="unfinished">Mowzuk:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="208"/>
        <source>Font Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="225"/>
        <source>Use custom font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="243"/>
        <source>Use Compact Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="252"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="260"/>
        <source>Use compact todo lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="270"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="277"/>
        <source>Override Scaling Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="284"/>
        <source>Scale Factor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="304"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="313"/>
        <source>Library Item Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="189"/>
        <source>System Tray:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="202"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="234"/>
        <source>Desktop Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="329"/>
        <source>Show notes excerpt in listings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="194"/>
        <source>Monochrome Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="143"/>
        <source>Custom Primary Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Pages/SettingsPage.qml" line="171"/>
        <source>Select</source>
        <translation type="unfinished">Saýlaň</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="165"/>
        <source>Custom Secondary Color:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackViewWindow</name>
    <message>
        <location filename="../qml/Windows/StackViewWindow.qml" line="21"/>
        <source>OpenTodoList</source>
        <translation type="unfinished">OpenTodoList</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="45"/>
        <source>Libraries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="70"/>
        <source>Add a new library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="89"/>
        <source>Accounts</source>
        <translation type="unfinished">Hasaplar</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="111"/>
        <source>Add an account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="42"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation>Kitaphanany sinhronlamakda ýalňyşlyklar boldy. Kitaphana sazlamalarynyň täzelenendigine göz ýetiriň.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="48"/>
        <source>Ignore</source>
        <translation>Üns berme</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="52"/>
        <source>View</source>
        <translation>Gör</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../qml/Widgets/TagsEditor.qml" line="32"/>
        <source>Add Tag</source>
        <translation>Bellik goşuň</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="136"/>
        <source>✔ No open todos - everything done</source>
        <translation>Open Açyk todos ýok - hemme zat ýerine ýetirildi</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="77"/>
        <source>Due on %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="166"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Gözleg termini 1, gözleg termini 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="253"/>
        <source>Todos</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="273"/>
        <source>Add new todo...</source>
        <translation>Täze todo goşuň ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="178"/>
        <source>Manually</source>
        <translation>El bilen</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="184"/>
        <source>Name</source>
        <translation>Ady</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="190"/>
        <source>Due Date</source>
        <translation>Berlen senesi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="196"/>
        <source>Created At</source>
        <translation>Döredildi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="202"/>
        <source>Updated At</source>
        <translation>Täzelendi</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="215"/>
        <source>Show Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="223"/>
        <source>Show At The End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="181"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Gözleg termini 1, gözleg termini 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="206"/>
        <source>Tasks</source>
        <translation>Wezipeler</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="212"/>
        <source>Add new task...</source>
        <translation>Täze wezipe goşuň ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="303"/>
        <source>Show Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="310"/>
        <source>Show At The End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../qml/Widgets/TodosWidget.qml" line="211"/>
        <source>Due on: %1</source>
        <translation>Berilmeli: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../qml/Widgets/TodosWidgetDelegate.qml" line="238"/>
        <source>More Actions...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation>OpenTodoList %1 täzelenmesi bar.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation>Üns berme</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation>Göçürip al</translation>
    </message>
</context>
</TS>
