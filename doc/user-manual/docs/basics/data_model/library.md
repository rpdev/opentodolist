# Library

Libraries are an important concept within OpenTodoList. As a user,
think of them like a _folder_ in your computer's file system, which
in turn contains the items you want to manage.

![A library in OpenTodoList](../../assets/screenshots/Android%20Tablet/library.png)

In fact, libraries are - in terms of storage - folders, but the structure within
them does not reflect one to one what you see in the user interface (as so
often, this is due to technical reasons and required for efficient working
of the app). However, what this immediately means: You can simply copy such
a library folder to e.g. back it up and restore it later. Note, though, that
if you want to sync your items across devices, you rather might want to use
one of the supported [Sync Mechanisms](../../features/sync/index.md).