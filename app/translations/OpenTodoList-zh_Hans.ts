<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hans">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation>关于...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="33"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="40"/>
        <source>A todo and task managing application.</source>
        <translation>一个待办事项和任务管理应用。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="68"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation>OpenTodoList 是根据&lt;a href=&apos;app-license&apos;&gt; GNU通用公共许可证&lt;/a&gt;第3版或（根据您的选择）任何更高版本的条款发行的。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="85"/>
        <source>Report an Issue</source>
        <translation>报告问题</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="114"/>
        <source>Third Party Libraries and Resources</source>
        <translation>第三方库和资源</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="151"/>
        <source>Author:</source>
        <translation>作者:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="156"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="163"/>
        <source>License:</source>
        <translation>许可:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="168"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="174"/>
        <source>Download:</source>
        <translation>下载：</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="178"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%1 &lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="92"/>
        <source>Copy System Information</source>
        <translation>复制系统信息</translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="20"/>
        <source>Select Account Type</source>
        <translation>选择账户类型</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="55"/>
        <source>Account Type</source>
        <translation>账户类型</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="61"/>
        <source>NextCloud</source>
        <translation>NextCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="64"/>
        <source>ownCloud</source>
        <translation>ownCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="67"/>
        <source>WebDAV</source>
        <translation>WebDAV</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="70"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../qml/Pages/AccountsPage.qml" line="18"/>
        <location filename="../qml/Pages/AccountsPage.qml" line="40"/>
        <source>Accounts</source>
        <translation>账户</translation>
    </message>
</context>
<context>
    <name>AllSubtasksDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="13"/>
        <source>Everything in %1 done! Do you want to mark it as well as done?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="30"/>
        <source>Keep Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="200"/>
        <source>Manage your personal data.</source>
        <translation>管理您的个人数据。</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="206"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation>打开一些针对触摸屏的优化。</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="209"/>
        <source>Only run the app background service</source>
        <translation>仅运行应用后台服务</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="211"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation>仅运行应用图形界面并连接至现有应用的后台服务</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="216"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation>在 Windows 上启用一个终端来获取调试输出</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="367"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="433"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="375"/>
        <source>Quick Note</source>
        <translation>快捷笔记</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="219"/>
        <source>Background Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="223"/>
        <source>App continues to sync your data in the background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="227"/>
        <source>Quit</source>
        <translation type="unfinished">退出</translation>
    </message>
</context>
<context>
    <name>ApplicationMenu</name>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="36"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="87"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="158"/>
        <source>Navigate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="197"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="18"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="27"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="35"/>
        <source>Accounts</source>
        <translation type="unfinished">账户</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="43"/>
        <source>Add Tag</source>
        <translation type="unfinished">添加标签</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="51"/>
        <source>Attach File</source>
        <translation type="unfinished">附加文件</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="59"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="66"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="73"/>
        <source>Copy</source>
        <translation type="unfinished">复制</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="81"/>
        <source>Copy Link To Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="89"/>
        <source>Create Sample Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="159"/>
        <source>Delete</source>
        <translation type="unfinished">删除</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="167"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished">删除已完成的项目</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="175"/>
        <source>Due Date</source>
        <translation type="unfinished">到期日期</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="183"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="190"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="207"/>
        <source>Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="213"/>
        <source>Mark all items as done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="225"/>
        <source>Mark all items as undone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="237"/>
        <source>Move</source>
        <translation type="unfinished">移动</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="245"/>
        <source>New Library</source>
        <translation type="unfinished">新资料库</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="253"/>
        <source>Open Created Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="260"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="268"/>
        <source>Open Library Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="274"/>
        <source>Quit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="280"/>
        <source>Rename</source>
        <translation type="unfinished">重命名</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="288"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="309"/>
        <source>Scroll to Top</source>
        <translation type="unfinished">滚动到顶端</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="316"/>
        <source>Scroll to Bottom</source>
        <translation type="unfinished">滚动到底端</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="323"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="330"/>
        <source>Set Progress</source>
        <translation type="unfinished">设定进度</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="337"/>
        <source>Sort</source>
        <translation type="unfinished">排序</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="345"/>
        <source>Sync Now</source>
        <translation type="unfinished">立即同步</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="358"/>
        <source>Sync Log</source>
        <translation type="unfinished">同步日志</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="369"/>
        <source>Translate The App...</source>
        <translation type="unfinished">帮助我们翻译这个应用...</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../qml/Components/ApplicationToolBar.qml" line="86"/>
        <source>Problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="36"/>
        <source>Attach File</source>
        <translation>附加文件</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="51"/>
        <source>Delete Attachment?</source>
        <translation>删除附件？</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="55"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation>您确定要删除附件&lt;strong&gt;％1 &lt;/ strong&gt;吗？此操作无法撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="69"/>
        <source>Attachments</source>
        <translation>附件</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation>白</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation>红</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation>绿</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation>蓝</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation>黄</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation>橙</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation>紫</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation>明亮</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation>黑暗</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../qml/Actions/CopyTodo.qml" line="12"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../qml/Actions/CopyTopLevelItem.qml" line="12"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
</context>
<context>
    <name>DeepLinkHandler</name>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="56"/>
        <source>Uuups... seems that&apos;s a dead end...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="65"/>
        <source>Sorry, that link does not lead to any page or item that is present on this device. Check if the library or item to which the link points is synchronized on this device and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="18"/>
        <source>Delete Account?</source>
        <translation>删除账户？</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="35"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation>您是否真的要删除账户&lt;strong&gt;%1&lt;/strong&gt;？这样会从您的设备中删除属于该账户的所有资料库吗？&lt;br/&gt;&lt;br/&gt;&lt;i&gt;注意: 您可以在服务器端通过添加回账户的方式恢复它们。&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../qml/Actions/DeleteCompletedChildren.qml" line="10"/>
        <source>Delete Completed Items</source>
        <translation>删除已完成的项目</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="18"/>
        <source>Delete Completed Items?</source>
        <translation>删除已完成的项目？</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除待办事项列表&lt;strong&gt;%1&lt;/strong&gt;中所有的已完成项目？这不能被撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="43"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除待办事项列表&lt;strong&gt;%1&lt;/strong&gt;中所有的已完成项目？这不能被撤消。</translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../qml/Actions/DeleteItem.qml" line="10"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="18"/>
        <source>Delete Item?</source>
        <translation>删除项目？</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="31"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除图像&lt;strong&gt;%1&lt;/strong&gt;？这不能被撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="35"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除待办事项列表&lt;strong&gt;%1&lt;/strong&gt;？这不能被撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除待办事项&lt;strong&gt;%1&lt;/strong&gt;？这不能被撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="43"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除任务&lt;strong&gt;%1&lt;/strong&gt;？这不能被撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="46"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除笔记 &lt;strong&gt;%1&lt;/strong&gt;？这不能被撤消。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="49"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>您是否真的要删除页面&lt;strong&gt;％1 &lt;/ strong&gt;？这不能被撤消。</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="18"/>
        <source>Delete Library?</source>
        <translation>删除资料库？</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="32"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation>您是否真的要从应用程序中删除资料库&lt;strong&gt;%1&lt;/strong&gt;？ &lt;em&gt;这将删除该资料库中的所有文件。&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="38"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation>您是否真的要从应用程序中删除资料库&lt;strong&gt;%1&lt;/strong&gt;？请注意，库中的文件不会被删除，因此您以后可以还原该库。</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="26"/>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="65"/>
        <source>Connection Settings</source>
        <translation type="unfinished">连接设定</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="73"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished">注册遇到困难？</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="84"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="91"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="95"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="104"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="110"/>
        <source>Copy Link</source>
        <translation type="unfinished">复制连接</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="114"/>
        <source>Copied!</source>
        <translation type="unfinished">已复制!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="123"/>
        <source>Name:</source>
        <translation type="unfinished">名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="130"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="22"/>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="57"/>
        <source>Edit Account</source>
        <translation>编辑账户</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="63"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="74"/>
        <source>Server Address:</source>
        <translation>服务器地址:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="81"/>
        <source>https://myserver.example.com</source>
        <translation>https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="89"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="102"/>
        <source>User:</source>
        <translation>用户:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="109"/>
        <source>User Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="115"/>
        <source>Password:</source>
        <translation>密码:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="122"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="131"/>
        <source>Disable Certificate Checks</source>
        <translation>禁用证书检查</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="140"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>未能连接到服务器。请检查您的用户名、密码和服务器地址，然后重试。</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="32"/>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="116"/>
        <source>Edit Account</source>
        <translation>编辑账户</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="122"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="133"/>
        <source>Server Address:</source>
        <translation>服务器地址:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="139"/>
        <source>https://myserver.example.com</source>
        <translation>https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="146"/>
        <source>User:</source>
        <translation>用户:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="152"/>
        <source>User Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="159"/>
        <source>Password:</source>
        <translation>密码:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="166"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="176"/>
        <source>Disable Certificate Checks</source>
        <translation>禁用证书检查</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="184"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>未能连接到服务器。请检查您的用户名、密码和服务器地址，然后重试。</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="68"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt;已创建。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="74"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="79"/>
        <source>Dismiss</source>
        <translation>解散</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="31"/>
        <source>Due on</source>
        <translation>到期于</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="69"/>
        <source>First due on %1.</source>
        <translation>首先到期于 %1。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="88"/>
        <source>No recurrence pattern set...</source>
        <translation>未设置重复模式...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="90"/>
        <source>Recurs every day.</source>
        <translation>每天重复。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="92"/>
        <source>Recurs every week.</source>
        <translation>每周重复。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="94"/>
        <source>Recurs every month.</source>
        <translation>每月重复。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="98"/>
        <source>Recurs every %1 days.</source>
        <translation>每 %1 天重复一次。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="96"/>
        <source>Recurs every year.</source>
        <translation>每年重复。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="101"/>
        <source>Recurs every %1 weeks.</source>
        <translation>每 %1 周重复。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="104"/>
        <source>Recurs every %1 months.</source>
        <translation>每 %1 月重复。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="141"/>
        <source>Recurs until %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="144"/>
        <source>Recurs indefinitely</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="28"/>
        <source>Notes</source>
        <translation>笔记</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="73"/>
        <source>No notes added yet - click here to add some.</source>
        <translation>尚未添加任何笔记 - 单击此处添加一些笔记。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="160"/>
        <source>Export to File...</source>
        <translation>导出到文件...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>Markdown files</source>
        <translation>Markdown 文件</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>All files</source>
        <translation>所有的文件</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="106"/>
        <source>Copy</source>
        <translation type="unfinished">复制</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="113"/>
        <source>Copy Formatted Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="129"/>
        <source>Copy Plain Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="190"/>
        <source>Move Todo Into...</source>
        <translation>移动待办事项至...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="220"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation>转换任务为待办事项并移动至...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="236"/>
        <source>Copy Item Into...</source>
        <translation>复制项目至...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="253"/>
        <source>Copy Todo Into...</source>
        <translation>复制待办事项至...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="205"/>
        <source>Move Task Into...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="55"/>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="223"/>
        <source>Schedule</source>
        <translation>时间表</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="85"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="78"/>
        <source>Edit List</source>
        <translation>编辑列表</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Hide Schedule</source>
        <translation>隐藏时间表</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Show Schedule</source>
        <translation>显示时间表</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="201"/>
        <source>Move Up</source>
        <translation>向上移动</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="210"/>
        <source>Move Down</source>
        <translation>向下移动</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="93"/>
        <source>Donate</source>
        <translation>捐助</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="293"/>
        <source>Untagged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Library</name>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="615"/>
        <source>Unable to create backup folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="619"/>
        <source>Failed to change into backup folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="630"/>
        <source>Failed to open file for writing: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="210"/>
        <source>Red</source>
        <translation>红</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="217"/>
        <source>Green</source>
        <translation>绿</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="224"/>
        <source>Blue</source>
        <translation>蓝</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="231"/>
        <source>Yellow</source>
        <translation>黄</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="238"/>
        <source>Orange</source>
        <translation>橙</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="245"/>
        <source>Lilac</source>
        <translation>紫</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="252"/>
        <source>White</source>
        <translation>白</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="262"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="273"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="355"/>
        <source>Note Title</source>
        <translation>笔记标题</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="368"/>
        <source>Todo List Title</source>
        <translation>待办事项清单标题</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="381"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>搜索词1，搜索词2，...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="458"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation>这里什么都没有！首先添加&lt;a href=&apos;#note&apos;&gt;便笺&lt;/a&gt;，&lt;a href=&apos;#todolist&apos;&gt;待办事项列表&lt;/a&gt;或&lt;a href=&apos;#image&apos;&gt;图片&lt;/a&gt;。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="514"/>
        <source>Sort By</source>
        <translation>排序方式</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="518"/>
        <source>Manually</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="525"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="532"/>
        <source>Due To</source>
        <translation>到期于</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="539"/>
        <source>Created At</source>
        <translation>创建于</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="546"/>
        <source>Updated At</source>
        <translation>更新于</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="267"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="285"/>
        <source>Select Image</source>
        <translation>选择图片</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="13"/>
        <source>Synchronization Log</source>
        <translation>同步日志</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="68"/>
        <source>Debugging information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="70"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="72"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="74"/>
        <source>Download</source>
        <translation type="unfinished">下载</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="76"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="78"/>
        <source>Create local folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="80"/>
        <source>Create remote folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="82"/>
        <source>Deleting locally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="84"/>
        <source>Deleting remotely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="86"/>
        <source>Unknown log message type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="23"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="197"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation>首先&lt;a href=&apos;#newLibrary&apos;&gt;创建一个新资料库&lt;/a&gt;。资料库用于存储各种项目，例如笔记、待办事项列表和图像。</translation>
    </message>
</context>
<context>
    <name>MarkFutureInstanceAsDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="13"/>
        <source>%1 is scheduled for the future - do you want to mark that future instance as done?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="29"/>
        <source>Keep Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveTask</name>
    <message>
        <location filename="../qml/Actions/MoveTask.qml" line="12"/>
        <source>Move</source>
        <translation type="unfinished">移动</translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../qml/Actions/MoveTodo.qml" line="12"/>
        <source>Move</source>
        <translation>移动</translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="53"/>
        <source>Connection Settings</source>
        <translation type="unfinished">连接设定</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="61"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished">注册遇到困难？</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="72"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="79"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="85"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="94"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="100"/>
        <source>Copy Link</source>
        <translation type="unfinished">复制连接</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="104"/>
        <source>Copied!</source>
        <translation type="unfinished">已复制!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="113"/>
        <source>Name:</source>
        <translation type="unfinished">名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="120"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="104"/>
        <source>Today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="105"/>
        <source>Tomorrow</source>
        <translation>明天</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="106"/>
        <source>This Week</source>
        <translation>本周</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="107"/>
        <source>Next Week</source>
        <translation>下周</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="108"/>
        <source>Select...</source>
        <translation>选择...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="138"/>
        <source>Title:</source>
        <translation>标题:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="143"/>
        <source>The title for your new item...</source>
        <translation>新项目的标题...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="185"/>
        <source>Create in:</source>
        <translation>创建于:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="226"/>
        <source>Due on:</source>
        <translation>到期于:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="149"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="81"/>
        <source>Create Library in Account</source>
        <translation>在账户中创建资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="90"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation>在账户中创建的资料库将与其同步。这样可以轻松地将资料库备份到服务器，然后再从那里还原它。此外，此资料库可以与其他用户共享（如果服务器允许的话）。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="98"/>
        <source>Existing Libraries</source>
        <translation>现有资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="106"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation>选择服务器上的现有资料库以将其添加到应用程序。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="112"/>
        <source>No libraries were found on the server.</source>
        <translation>在服务器上找不到资料库。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="121"/>
        <source>Searching existing libraries...</source>
        <translation>搜索现有资料库...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="164"/>
        <source>Create a New Library</source>
        <translation>创建一个新资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="172"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation>创建一个新的资料库，它将与服务器同步。这样的资料库也可以添加到其他设备上的应用程序中以同步数据。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="186"/>
        <source>My new library&apos;s name</source>
        <translation>我的新资料库名字</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="100"/>
        <source>Open a Folder as a Library</source>
        <translation>按照资料库打开一个文件夹</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="108"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation>您可以使用任何文件夹作为资料库的位置。&lt;br/&gt; &lt;br/&gt;当您想使用其他工具（例如云提供商的同步客户端）将数据与服务器同步时，此功能特别有用。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="112"/>
        <source>Folder:</source>
        <translation>文件夹:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="117"/>
        <source>Path to a folder to use as a library</source>
        <translation>用作资料库的文件夹的路径</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="123"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="128"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="133"/>
        <source>My Local Library Name</source>
        <translation>我的本地资料库名称</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation>选择一个文件夹</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="22"/>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="62"/>
        <source>Create Library</source>
        <translation>创建资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="69"/>
        <source>Local Library</source>
        <translation>本地资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="76"/>
        <source>Use Folder as Library</source>
        <translation>将文件夹用作资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="88"/>
        <source>Add Libraries From Your Accounts</source>
        <translation>从您的帐户添加资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="115"/>
        <source>Add Account</source>
        <translation>添加账户</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="61"/>
        <source>Create a Local Library</source>
        <translation>创建本地资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="69"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation>本地资料库仅存储在您的设备上-这非常适合隐私方面的考量！&lt;br/&gt; &lt;br/&gt;当您只想在本地存储信息并通过其他机制定期备份所有数据时，可以使用它。如果需要跨多个设备访问信息，请创建一个同步的资料库。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="79"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="84"/>
        <source>My Local Library Name</source>
        <translation>我的本地资料库名称</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="19"/>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="71"/>
        <source>Connection Settings</source>
        <translation>连接设定</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="77"/>
        <source>Server Address:</source>
        <translation>服务器地址:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="107"/>
        <source>Trouble Signing In?</source>
        <translation>注册遇到困难？</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="118"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation>我们已尝试打开您的浏览器来登录您的 NextCloud 实例。请登录并授予 OpenTodoList 访问权限来继续。在浏览器中访问您的 NextCloud 遇到困难？您也可以手动输入您的用户名和密码。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="126"/>
        <source>Log in Manually</source>
        <translation>手动登录</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="132"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation>理想情况下，您可以使用特定的应用程序密码而不是您的用户密码。在您使用二次因素认证(2FA)保护您的登录的情况下，您甚至必须使用应用程序特定密码来访问您的 NextCloud。 您可以在您的用户设定中创建这类密码。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="141"/>
        <source>Create App Password</source>
        <translation>创建应用程序密码</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="150"/>
        <source>Account Settings</source>
        <translation>账户设定</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="160"/>
        <source>Copy Link</source>
        <translation>复制连接</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="166"/>
        <source>Copied!</source>
        <translation>已复制!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="175"/>
        <source>User:</source>
        <translation>用户:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="182"/>
        <source>User Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="189"/>
        <source>Password:</source>
        <translation>密码:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="196"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="206"/>
        <source>Disable Certificate Checks</source>
        <translation>禁用证书检查</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="210"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="223"/>
        <source>Account Name</source>
        <translation>账户名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="233"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>未能连接到服务器。请检查您的用户名、密码和服务器地址，然后重试。</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="52"/>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="41"/>
        <source>Todo List</source>
        <translation>待办事项清单</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="56"/>
        <source>Image</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="46"/>
        <source>Todo</source>
        <translation>待办事项</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="161"/>
        <source>Account Name</source>
        <translation>账户名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="28"/>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="99"/>
        <source>Connection Settings</source>
        <translation>连接设定</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="105"/>
        <source>Server Address:</source>
        <translation>服务器地址:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="111"/>
        <source>https://myserver.example.com</source>
        <translation>https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="117"/>
        <source>User:</source>
        <translation>用户:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="123"/>
        <source>User Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="129"/>
        <source>Password:</source>
        <translation>密码:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="135"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="144"/>
        <source>Disable Certificate Checks</source>
        <translation>禁用证书检查</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="148"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="171"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>未能连接到服务器。请检查您的用户名、密码和服务器地址，然后重试。</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../qml/Widgets/NoteItem.qml" line="91"/>
        <source>Due on %1</source>
        <translation>到期于 %1</translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="171"/>
        <source>Main Page</source>
        <translation>主页</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="238"/>
        <source>New Page</source>
        <translation>新一页</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation type="unfinished">系统语言</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation>账户缺少密钥</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="18"/>
        <location filename="../qml/Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation>检测到问题</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation>同步失败项目至资料库</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="91"/>
        <source>Retry Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../qml/Actions/PromoteTask.qml" line="13"/>
        <source>Promote</source>
        <translation>推荐</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="312"/>
        <location filename="../appstartup.cpp" line="319"/>
        <source>unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="17"/>
        <source>Quick Notes</source>
        <translation>快捷笔记</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="36"/>
        <source>Quick Notes Editor</source>
        <translation>快捷笔记编辑器</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="45"/>
        <source>Open the main window</source>
        <translation>打开主窗口</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="57"/>
        <source>Quick Note Title</source>
        <translation>快捷笔记标题</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="121"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="126"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation>保存输入的笔记至选定的资料库。点按并保持来获得更多的保存方式。</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="148"/>
        <source>Save as Note</source>
        <translation>按照笔记保存</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="161"/>
        <source>Save as Todo List</source>
        <translation>以待办事项清单保存</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="165"/>
        <source>Quick Todo List</source>
        <translation>快捷待办事项清单</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="152"/>
        <source>Quick Note</source>
        <translation>快捷笔记</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="70"/>
        <source>Type your notes here...</source>
        <translation>在这里输入您的笔记...</translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="29"/>
        <source>Edit Recurrence</source>
        <translation>编辑重复</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation>决不</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="47"/>
        <source>Daily</source>
        <translation>每日</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="51"/>
        <source>Weekly</source>
        <translation>每周</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="55"/>
        <source>Monthly</source>
        <translation>每月</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="63"/>
        <source>Every N Days</source>
        <translation>每 N 天</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="97"/>
        <source>Recurs:</source>
        <translation>重复出现:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="115"/>
        <source>Number of days:</source>
        <translation>天数:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="141"/>
        <source>Recur relative to the date when marking as done</source>
        <translation>相对于标记为完成的日期重复</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="59"/>
        <source>Yearly</source>
        <translation>每年</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="67"/>
        <source>Every N Weeks</source>
        <translation>每 N 周</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="71"/>
        <source>Every N Months</source>
        <translation>每 N 月</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="117"/>
        <source>Number of weeks:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="119"/>
        <source>Number of months:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../qml/Actions/RenameItem.qml" line="10"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="21"/>
        <source>Rename Item</source>
        <translation>重命名项目</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="32"/>
        <source>Enter item title...</source>
        <translation>输入项目标题...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="19"/>
        <source>Rename Library</source>
        <translation>重命名资料库</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="36"/>
        <source>Enter library title...</source>
        <translation>输入资料库标题...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../qml/Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="147"/>
        <source>Today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="148"/>
        <source>Tomorrow</source>
        <translation>明天</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="184"/>
        <source>Later This Week</source>
        <translation>本周晚些时候</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="186"/>
        <source>Next Week</source>
        <translation>下周</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="187"/>
        <source>Coming Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="266"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation>尚未安排任何时间...在项目中添加截止日期，以便它们出现在此处。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="123"/>
        <source>Overdue</source>
        <translation>逾期</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="52"/>
        <source>Schedule</source>
        <translation type="unfinished">时间表</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/SelectLibraryDialog.qml" line="21"/>
        <source>Select Library</source>
        <translation>选择图书馆</translation>
    </message>
</context>
<context>
    <name>SelectTodoDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="22"/>
        <source>Select Todo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="42"/>
        <source>Todo List:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="64"/>
        <source>Todo:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoListDialog.qml" line="24"/>
        <source>Select Todo List</source>
        <translation>选择待办事详清单</translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTopLevelItemDialog.qml" line="21"/>
        <source>Select Item</source>
        <translation>选择项目</translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation>定于本周到期</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation>定于下周到期</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../qml/Actions/SetDueTo.qml" line="10"/>
        <source>Select Due Date</source>
        <translation>选择到期日</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../qml/Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation>定于今天到期</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../qml/Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation>定于明日到期</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../qml/Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation>设定进度</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="46"/>
        <source>User Interface</source>
        <translation>用户界面</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="54"/>
        <source>Language:</source>
        <translation>语言:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="118"/>
        <source>Theme:</source>
        <translation>主题:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="208"/>
        <source>Font Size:</source>
        <translation>字体尺寸:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="225"/>
        <source>Use custom font size</source>
        <translation>使用自定义字体尺寸</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="243"/>
        <source>Use Compact Style</source>
        <translation>使用紧凑风格</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="252"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation>缩小组件之间的间距并缩小字体尺寸。

&lt;em&gt;需要重新启动应用。&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="260"/>
        <source>Use compact todo lists</source>
        <translation>使用紧凑的待办事项清单</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="270"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation>缩小待办事项和任务列表的边距使得屏幕上可以显示更多项目。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="277"/>
        <source>Override Scaling Factor</source>
        <translation>覆盖缩放级别</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="284"/>
        <source>Scale Factor:</source>
        <translation>缩放级别:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="304"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation>使用此来手动缩放用户界面。默认情况下，应用程序应该可以根据您的设备配置自动适应。如果这并未如预期般有效，您可以在这里设定一个自定义的缩放级别。

这需要重新启动应用。</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="313"/>
        <source>Library Item Size:</source>
        <translation>资料库项目尺寸:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="189"/>
        <source>System Tray:</source>
        <translation>系统托盘:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="202"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation>点击以打开快捷笔记编辑器</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="234"/>
        <source>Desktop Mode</source>
        <translation>桌面模式</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="329"/>
        <source>Show notes excerpt in listings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="194"/>
        <source>Monochrome Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="143"/>
        <source>Custom Primary Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Pages/SettingsPage.qml" line="171"/>
        <source>Select</source>
        <translation type="unfinished">选择</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="165"/>
        <source>Custom Secondary Color:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackViewWindow</name>
    <message>
        <location filename="../qml/Windows/StackViewWindow.qml" line="21"/>
        <source>OpenTodoList</source>
        <translation type="unfinished">OpenTodoList</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="45"/>
        <source>Libraries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="70"/>
        <source>Add a new library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="89"/>
        <source>Accounts</source>
        <translation type="unfinished">账户</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="111"/>
        <source>Add an account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="42"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation>同步资料库时出错。请确保资料库设置是最新的。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="48"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="52"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../qml/Widgets/TagsEditor.qml" line="32"/>
        <source>Add Tag</source>
        <translation>添加标签</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="136"/>
        <source>✔ No open todos - everything done</source>
        <translation>✔没有待办事项-一切都完成了</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="77"/>
        <source>Due on %1</source>
        <translation>到期于 %1</translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="166"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>搜索词1，搜索词2，...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="253"/>
        <source>Todos</source>
        <translation>待办事项</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="273"/>
        <source>Add new todo...</source>
        <translation>添加新的待办事项...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="178"/>
        <source>Manually</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="184"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="190"/>
        <source>Due Date</source>
        <translation>到期日期</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="196"/>
        <source>Created At</source>
        <translation>创建于</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="202"/>
        <source>Updated At</source>
        <translation>更新于</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="215"/>
        <source>Show Completed</source>
        <translation>选择已完成</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="223"/>
        <source>Show At The End</source>
        <translation>显示在末尾</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="181"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>搜索词1，搜索词2，...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="206"/>
        <source>Tasks</source>
        <translation>任务</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="212"/>
        <source>Add new task...</source>
        <translation>添加新任务...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="303"/>
        <source>Show Completed</source>
        <translation>显示已完成</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="310"/>
        <source>Show At The End</source>
        <translation>显示在末尾</translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../qml/Widgets/TodosWidget.qml" line="211"/>
        <source>Due on: %1</source>
        <translation>到期于: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../qml/Widgets/TodosWidgetDelegate.qml" line="238"/>
        <source>More Actions...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation>一个 OpenTodoList %1的新版本可供使用。</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation>下载</translation>
    </message>
</context>
</TS>
