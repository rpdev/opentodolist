<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation>Про додаток</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="33"/>
        <source>OpenTodoList</source>
        <translation>OpenTodoList</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="40"/>
        <source>A todo and task managing application.</source>
        <translation>Додаток для керування діями та завданнями.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="68"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation>OpenTodoList випущено згідно з умовами &lt;a href=&apos;app-license&apos;&gt;Загальної публічної ліцензії GNU&lt;/a&gt; версії 3 або (на ваш вибір) будь-якої пізнішої версії.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="85"/>
        <source>Report an Issue</source>
        <translation>Повідомити про проблему</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="114"/>
        <source>Third Party Libraries and Resources</source>
        <translation>Бібліотеки та ресурси сторонніх розробників</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="151"/>
        <source>Author:</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="156"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="163"/>
        <source>License:</source>
        <translation>Ліцензія</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="168"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="174"/>
        <source>Download:</source>
        <translation>Завантажити</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="178"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AboutPage.qml" line="92"/>
        <source>Copy System Information</source>
        <translation>Копиювати системну інформацію</translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="20"/>
        <source>Select Account Type</source>
        <translation>Виберіть тип облікового запису</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="55"/>
        <source>Account Type</source>
        <translation>Тип Акаунту</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="61"/>
        <source>NextCloud</source>
        <translation>NextCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="64"/>
        <source>ownCloud</source>
        <translation>ownCloud</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="67"/>
        <source>WebDAV</source>
        <translation>WebDAV</translation>
    </message>
    <message>
        <location filename="../qml/Pages/AccountTypeSelectionPage.qml" line="70"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../qml/Pages/AccountsPage.qml" line="18"/>
        <location filename="../qml/Pages/AccountsPage.qml" line="40"/>
        <source>Accounts</source>
        <translation>Облікові записи</translation>
    </message>
</context>
<context>
    <name>AllSubtasksDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="13"/>
        <source>Everything in %1 done! Do you want to mark it as well as done?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/AllSubtasksDone.qml" line="30"/>
        <source>Keep Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="200"/>
        <source>Manage your personal data.</source>
        <translation>Керуйте своїми особистими даними.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="206"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation>Увімкніть деякі оптимізації для сенсорних екранів.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="209"/>
        <source>Only run the app background service</source>
        <translation>Запустіть лише фонову службу програми</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="211"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation>Запустіть лише графічний інтерфейс програми та підключіться до існуючої фонової служби програми</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="216"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation>Увімкніть консоль у Windows, щоб отримати вихідні дані налагодження</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="367"/>
        <source>Open</source>
        <translation>Відкрити</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="433"/>
        <source>Quit</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="375"/>
        <source>Quick Note</source>
        <translation>Швидка Нотатка</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="219"/>
        <source>Background Sync</source>
        <translation>Фонова Синхронізація</translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="223"/>
        <source>App continues to sync your data in the background</source>
        <translation>Додаток продовжує синхронізувати ваші дані у фоновому режимі</translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="227"/>
        <source>Quit</source>
        <translation>Вийти</translation>
    </message>
</context>
<context>
    <name>ApplicationMenu</name>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="36"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="87"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="158"/>
        <source>Navigate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ApplicationMenu.qml" line="197"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="18"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="27"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="35"/>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="43"/>
        <source>Add Tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="51"/>
        <source>Attach File</source>
        <translation type="unfinished">Прикріпити файл</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="59"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="66"/>
        <source>Close</source>
        <translation type="unfinished">Закрити</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="73"/>
        <source>Copy</source>
        <translation type="unfinished">Копіювати</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="81"/>
        <source>Copy Link To Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="89"/>
        <source>Create Sample Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="159"/>
        <source>Delete</source>
        <translation type="unfinished">Видалити</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="167"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished">Видалити завершені елементи</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="175"/>
        <source>Due Date</source>
        <translation type="unfinished">Термін виконання</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="183"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="190"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="207"/>
        <source>Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="213"/>
        <source>Mark all items as done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="225"/>
        <source>Mark all items as undone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="237"/>
        <source>Move</source>
        <translation type="unfinished">Рухатися</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="245"/>
        <source>New Library</source>
        <translation type="unfinished">Нова бібліотека</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="253"/>
        <source>Open Created Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="260"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="268"/>
        <source>Open Library Folder</source>
        <translation type="unfinished">Відкрити Папку Бібліотеки</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="274"/>
        <source>Quit</source>
        <translation type="unfinished">Вийти</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="280"/>
        <source>Rename</source>
        <translation type="unfinished">Перейменувати</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="288"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="309"/>
        <source>Scroll to Top</source>
        <translation type="unfinished">Перегорнути Нагору</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="316"/>
        <source>Scroll to Bottom</source>
        <translation type="unfinished">Перегорнути Вниз</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="323"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="330"/>
        <source>Set Progress</source>
        <translation type="unfinished">Встановити прогрес</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="337"/>
        <source>Sort</source>
        <translation type="unfinished">Сортувати</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="345"/>
        <source>Sync Now</source>
        <translation type="unfinished">Синхронізувати зараз</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="358"/>
        <source>Sync Log</source>
        <translation type="unfinished">Журнал синхронізації</translation>
    </message>
    <message>
        <location filename="../qml/Components/ApplicationShortcuts.qml" line="369"/>
        <source>Translate The App...</source>
        <translation type="unfinished">Перекласти додаток...</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../qml/Components/ApplicationToolBar.qml" line="86"/>
        <source>Problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="36"/>
        <source>Attach File</source>
        <translation>Прикріпити файл</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="51"/>
        <source>Delete Attachment?</source>
        <translation>Видалити вкладення?</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="55"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation>Ви впевнені, що хочете видалити вкладення &lt;strong&gt;%1&lt;/strong&gt;? Це вже не можна буде скасувати.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/Attachments.qml" line="69"/>
        <source>Attachments</source>
        <translation>Вкладення</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation>Колір</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation>Білий</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation>Червоний</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation>Зелений</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation>Синій</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation>Жовтий</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation>Оранжевий</translation>
    </message>
    <message>
        <location filename="../qml/Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation>Бузковий</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation>Світла</translation>
    </message>
    <message>
        <location filename="../qml/Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation>Темний</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation>Копія</translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../qml/Actions/CopyTodo.qml" line="12"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../qml/Actions/CopyTopLevelItem.qml" line="12"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
</context>
<context>
    <name>DeepLinkHandler</name>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="56"/>
        <source>Uuups... seems that&apos;s a dead end...</source>
        <translation>Упс... здається, це глухий кут...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/DeepLinkHandler.qml" line="65"/>
        <source>Sorry, that link does not lead to any page or item that is present on this device. Check if the library or item to which the link points is synchronized on this device and try again.</source>
        <translation>Вибачте, це посилання не веде на жодну сторінку або елемент, які є на цьому пристрої. Перевірте, чи бібліотеку або елемент, на який вказує посилання, синхронізовано на цьому пристрої, і повторіть спробу.</translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="18"/>
        <source>Delete Account?</source>
        <translation>Видалити Акаунт?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteAccountDialog.qml" line="35"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation>Ви дійсно хочете видалити обліковий запис &lt;strong&gt;%1&lt;/strong&gt;? Це призведе до видалення всіх бібліотек, що належать до облікового запису, з вашого пристрою?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Примітка. Ви можете відновити їх із сервера, знову додавши обліковий запис.&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../qml/Actions/DeleteCompletedChildren.qml" line="10"/>
        <source>Delete Completed Items</source>
        <translation>Видалити завершені елементи</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="18"/>
        <source>Delete Completed Items?</source>
        <translation>Видалити завершені елементи?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви справді бажаєте видалити всі виконані завдання зі списку справ &lt;strong&gt;%1&lt;/strong&gt;? Це не може бути скасоване.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteCompletedItemsDialog.qml" line="43"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви справді бажаєте видалити всі виконані завдання в списку завдань &lt;strong&gt;%1&lt;/strong&gt;? Це не може бути скасоване.</translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../qml/Actions/DeleteItem.qml" line="10"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="18"/>
        <source>Delete Item?</source>
        <translation>Видалити елемент?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="31"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви дійсно хочете видалити зображення &lt;strong&gt;%1&lt;/strong&gt;? Це вже не можна буде скасувати.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="35"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви дійсно хочете видалити список дій &lt;strong&gt;%1&lt;/strong&gt;? Це вже не можна буде скасувати.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви дійсно хочете видалити дію &lt;strong&gt;%1&lt;/strong&gt;? Це вже не можна буде скасувати.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="43"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви дійсно хочете видалити завдання &lt;strong&gt;%1&lt;/strong&gt;? Це вже не можна буде скасувати.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="46"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви дійсно хочете видалити замітку &lt;strong&gt;%1&lt;/strong&gt;? Це вже не можна буде скасувати.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteItemDialog.qml" line="49"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation>Ви дійсно хочете видалити сторінку &lt;strong&gt;%1&lt;/strong&gt;? Це не може бути скасоване.</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="18"/>
        <source>Delete Library?</source>
        <translation>Видалити бібліотеку?</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="32"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation>Ви дійсно хочете видалити бібліотеку з додатком &lt;strong&gt;%1&lt;/strong&gt;? &lt;em&gt;Це видалить усі файли, що належать до бібліотеки.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Windows/DeleteLibraryDialog.qml" line="38"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation>Ви дійсно хочете видалити бібліотеку з додатком &lt;strong&gt;%1&lt;/strong&gt;? Зауважте, що файли всередині бібліотеки не будуть видалені, тому згодом ви можете відновити бібліотеку.</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="26"/>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="65"/>
        <source>Connection Settings</source>
        <translation>Параметри Підключення</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="73"/>
        <source>Trouble Signing In?</source>
        <translation>Проблеми З Входом?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="84"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation>Ми спробували відкрити ваш браузер, щоб увійти у ваш акаунт Dropbox. Будь ласка, увійдіть і надайте доступ до OpenTodoList, щоб продовжити.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="91"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation>Ваш браузер не відкрився? Ви можете спробувати відкрити його ще раз або скопіювати потрібну URL-адресу вручну у ваш браузер.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="95"/>
        <source>Authorize...</source>
        <translation>Авторизувати...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="104"/>
        <source>Open Browser</source>
        <translation>Відкрити Браузер</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="110"/>
        <source>Copy Link</source>
        <translation>Скопіювати силку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="114"/>
        <source>Copied!</source>
        <translation>Скопійовано</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="123"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditDropboxAccountPage.qml" line="130"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="22"/>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="57"/>
        <source>Edit Account</source>
        <translation>Редагувати обліковий запис</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="63"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="74"/>
        <source>Server Address:</source>
        <translation>Адреса сервера:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="81"/>
        <source>https://myserver.example.com</source>
        <translation>https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="89"/>
        <source>Login</source>
        <translation>Логін</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="102"/>
        <source>User:</source>
        <translation>Користувач:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="109"/>
        <source>User Name</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="115"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="122"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="131"/>
        <source>Disable Certificate Checks</source>
        <translation>Вимкнути перевірку сертифікатів</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditNextCloudAccountPage.qml" line="140"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Не вдалося підключитися до сервера. Будь ласка, перевірте ваше ім&apos;я користувача, пароль і адресу сервера та повторіть спробу.</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="32"/>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="116"/>
        <source>Edit Account</source>
        <translation>Редагувати обліковий запис</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="122"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="133"/>
        <source>Server Address:</source>
        <translation>Адреса сервера:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="139"/>
        <source>https://myserver.example.com</source>
        <translation>https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="146"/>
        <source>User:</source>
        <translation>Користувач:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="152"/>
        <source>User Name</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="159"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="166"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="176"/>
        <source>Disable Certificate Checks</source>
        <translation>Вимкнути перевірку сертифікатів</translation>
    </message>
    <message>
        <location filename="../qml/Pages/EditWebDAVAccountPage.qml" line="184"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Не вдалося підключитися до сервера. Будь ласка, перевірте ваше ім&apos;я користувача, пароль і адресу сервера та повторіть спробу.</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="68"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt; було створено.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="74"/>
        <source>Open</source>
        <translation>Відкрити</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemCreatedNotification.qml" line="79"/>
        <source>Dismiss</source>
        <translation>Відхилити</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="31"/>
        <source>Due on</source>
        <translation>Термін на</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="69"/>
        <source>First due on %1.</source>
        <translation>Перший строк до %1.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="88"/>
        <source>No recurrence pattern set...</source>
        <translation>Шаблон повторення не встановлено...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="90"/>
        <source>Recurs every day.</source>
        <translation>Повторюється щодня.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="92"/>
        <source>Recurs every week.</source>
        <translation>Повторюється щотижня.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="94"/>
        <source>Recurs every month.</source>
        <translation>Повторюється кожного місяця.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="98"/>
        <source>Recurs every %1 days.</source>
        <translation>Повторюється кожні %1 днів.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="96"/>
        <source>Recurs every year.</source>
        <translation>Повторюється щороку.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="101"/>
        <source>Recurs every %1 weeks.</source>
        <translation>Повторюється кожні %1 тижні.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="104"/>
        <source>Recurs every %1 months.</source>
        <translation>Повторюється кожні %1 місяці.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="141"/>
        <source>Recurs until %1.</source>
        <translation>Повторюється до %1.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemDueDateEditor.qml" line="144"/>
        <source>Recurs indefinitely</source>
        <translation>Повторюється нескінченно</translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="28"/>
        <source>Notes</source>
        <translation>Нотатки</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="73"/>
        <source>No notes added yet - click here to add some.</source>
        <translation>Нотаток ще не додано - натисніть тут, щоб додати.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="160"/>
        <source>Export to File...</source>
        <translation>Експорт у файл...</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>Markdown files</source>
        <translation>Файли Markdown</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="163"/>
        <source>All files</source>
        <translation>Всі файли</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="106"/>
        <source>Copy</source>
        <translation type="unfinished">Копіювати</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="113"/>
        <source>Copy Formatted Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Widgets/ItemNotesEditor.qml" line="129"/>
        <source>Copy Plain Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="190"/>
        <source>Move Todo Into...</source>
        <translation>Перемістити завдання в...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="220"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation>Перетворіть Завдання на Справу і перемістіть в...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="236"/>
        <source>Copy Item Into...</source>
        <translation>Скопіювати Елемент В...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="253"/>
        <source>Copy Todo Into...</source>
        <translation>Скопіювати Справо В...</translation>
    </message>
    <message>
        <location filename="../qml/Utils/ItemUtils.qml" line="205"/>
        <source>Move Task Into...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="55"/>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="223"/>
        <source>Schedule</source>
        <translation>Список</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="85"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="78"/>
        <source>Edit List</source>
        <translation>Редагувати список</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Hide Schedule</source>
        <translation>Приховати розклад</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="190"/>
        <source>Show Schedule</source>
        <translation>Показати розклад</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="201"/>
        <source>Move Up</source>
        <translation>Рухатися вгору</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="210"/>
        <source>Move Down</source>
        <translation>Рухатися вниз</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="93"/>
        <source>Donate</source>
        <translation>Пожертвування</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/LibrariesSideBar.qml" line="293"/>
        <source>Untagged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Library</name>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="615"/>
        <source>Unable to create backup folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="619"/>
        <source>Failed to change into backup folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/datamodel/library.cpp" line="630"/>
        <source>Failed to open file for writing: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="210"/>
        <source>Red</source>
        <translation>Червоний</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="217"/>
        <source>Green</source>
        <translation>Зелений
</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="224"/>
        <source>Blue</source>
        <translation>Блакитний
</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="231"/>
        <source>Yellow</source>
        <translation>Жовтий</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="238"/>
        <source>Orange</source>
        <translation>Помаранчевий</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="245"/>
        <source>Lilac</source>
        <translation>Бузковий</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="252"/>
        <source>White</source>
        <translation>Білий</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="262"/>
        <source>Rename</source>
        <translation>Перейменувати</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="273"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="355"/>
        <source>Note Title</source>
        <translation>Заголовок примітки </translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="368"/>
        <source>Todo List Title</source>
        <translation>Заголовок списку справ </translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="381"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Пошук терміну 1, пошук терміну 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="458"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation>Тут нічого ще немає! Почніть з додавання &lt;a href=&apos;#note&apos;&gt;примітка &lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;список справ&lt;/a&gt; або &lt;a href=&apos;#image&apos;&gt;зображення&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="514"/>
        <source>Sort By</source>
        <translation>Сортувати за</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="518"/>
        <source>Manually</source>
        <translation>Вручну</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="525"/>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="532"/>
        <source>Due To</source>
        <translation>Через</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="539"/>
        <source>Created At</source>
        <translation>Створено в</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="546"/>
        <source>Updated At</source>
        <translation>Оновлено в</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="267"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LibraryPage.qml" line="285"/>
        <source>Select Image</source>
        <translation>Виберіть Зображення</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="13"/>
        <source>Synchronization Log</source>
        <translation>Журнал синхронізації</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="68"/>
        <source>Debugging information</source>
        <translation>Налагоджувальна інформація</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="70"/>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="72"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="74"/>
        <source>Download</source>
        <translation>Скачати</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="76"/>
        <source>Upload</source>
        <translation>Завантажити</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="78"/>
        <source>Create local folder</source>
        <translation>Створити локальну папку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="80"/>
        <source>Create remote folder</source>
        <translation>Створити віддалену папку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="82"/>
        <source>Deleting locally</source>
        <translation>Локальне видалення</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="84"/>
        <source>Deleting remotely</source>
        <translation>Віддалене видалення</translation>
    </message>
    <message>
        <location filename="../qml/Pages/LogViewPage.qml" line="86"/>
        <source>Unknown log message type</source>
        <translation>Невідомий тип повідомлення журналу</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="23"/>
        <source>OpenTodoList</source>
        <translation>Відкрити  TodoList</translation>
    </message>
    <message>
        <location filename="../qml/Windows/MainWindow.qml" line="197"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation>Почніть з &lt;a href=&apos;#newLibrary&apos;&gt;створення нової бібліотеки&lt;/a&gt;. Бібліотеки використовуються для зберігання різних видів предметів, таких як нотатки, списки справ та зображення.</translation>
    </message>
</context>
<context>
    <name>MarkFutureInstanceAsDone</name>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="13"/>
        <source>%1 is scheduled for the future - do you want to mark that future instance as done?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="20"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Components/Tooltips/MarkFutureInstanceAsDone.qml" line="29"/>
        <source>Keep Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveTask</name>
    <message>
        <location filename="../qml/Actions/MoveTask.qml" line="12"/>
        <source>Move</source>
        <translation type="unfinished">Рухатися</translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../qml/Actions/MoveTodo.qml" line="12"/>
        <source>Move</source>
        <translation>Рухатися</translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="53"/>
        <source>Connection Settings</source>
        <translation>Параметри Підключення</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="61"/>
        <source>Trouble Signing In?</source>
        <translation>Проблеми З Входом?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="72"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation>Ми спробували відкрити ваш браузер, щоб увійти у ваш акаунт Dropbox. Будь ласка, увійдіть і надайте доступ до OpenTodoList, щоб продовжити.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="79"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation>Ваш браузер не відкрився? Ви можете спробувати відкрити його ще раз або скопіювати потрібну URL-адресу вручну у ваш браузер.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="85"/>
        <source>Authorize...</source>
        <translation>Авторизувати...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="94"/>
        <source>Open Browser</source>
        <translation>Відкрити Браузер</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="100"/>
        <source>Copy Link</source>
        <translation>Копіювати Посилання</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="104"/>
        <source>Copied!</source>
        <translation>Скопійовано!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="113"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewDropboxAccountPage.qml" line="120"/>
        <source>Dropbox</source>
        <translation>Dropbox</translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="104"/>
        <source>Today</source>
        <translation>Сьогодні</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="105"/>
        <source>Tomorrow</source>
        <translation>Завтра</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="106"/>
        <source>This Week</source>
        <translation>Цього тижня</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="107"/>
        <source>Next Week</source>
        <translation>Наступного тижня</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="108"/>
        <source>Select...</source>
        <translation>Виберіть...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="138"/>
        <source>Title:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="143"/>
        <source>The title for your new item...</source>
        <translation>Назва вашого нового елемента...</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="185"/>
        <source>Create in:</source>
        <translation>Створити в:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="226"/>
        <source>Due on:</source>
        <translation>Дата виконання:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/NewItemWithDueDateDialog.qml" line="149"/>
        <source>Library</source>
        <translation>Бібліотека</translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="81"/>
        <source>Create Library in Account</source>
        <translation>Створіть бібліотеку в обліковому записі</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="90"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation>Бібліотека, створена в обліковому записі, синхронізується з ним. Це дозволяє легко створити резервну копію бібліотеки на сервері, а потім відновити її звідти. Крім того, такими бібліотеками можна ділитися з іншими користувачами (якщо це дозволяє сервер).</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="98"/>
        <source>Existing Libraries</source>
        <translation>Існуючі бібліотеки</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="106"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation>Виберіть існуючу бібліотеку на сервері, щоб додати її до програми.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="112"/>
        <source>No libraries were found on the server.</source>
        <translation>На сервері не знайдено бібліотек.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="121"/>
        <source>Searching existing libraries...</source>
        <translation>Пошук в існуючих бібліотеках...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="164"/>
        <source>Create a New Library</source>
        <translation>Створіть нову бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="172"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation>Створіть нову бібліотеку, яка буде синхронізована з сервером. Таку бібліотеку можна додати до програми на інших пристроях, а також для синхронізації даних.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryFromAccountPage.qml" line="186"/>
        <source>My new library&apos;s name</source>
        <translation>Назва моєї нової бібліотеки</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="100"/>
        <source>Open a Folder as a Library</source>
        <translation>Відкрийте папку як бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="108"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation>Ви можете використовувати будь-яку папку як місце для бібліотеки.&lt;br/&gt;&lt;br/&gt;Це особливо корисно, коли ви хочете використовувати інший інструмент (наприклад, клієнт синхронізації хмарного постачальника) для синхронізації даних із сервером.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="112"/>
        <source>Folder:</source>
        <translation>Папка:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="117"/>
        <source>Path to a folder to use as a library</source>
        <translation>Шлях до папки для використання як бібліотеки</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="123"/>
        <source>Select</source>
        <translation>Виберіть</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="128"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="133"/>
        <source>My Local Library Name</source>
        <translation>Назва моєї локальної бібліотеки</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation>Виберіть Папку</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="22"/>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="62"/>
        <source>Create Library</source>
        <translation>Створити бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="69"/>
        <source>Local Library</source>
        <translation>Локальна бібліотека</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="76"/>
        <source>Use Folder as Library</source>
        <translation>Використовувати папку як бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="88"/>
        <source>Add Libraries From Your Accounts</source>
        <translation>Додайте бібліотеки зі своїх облікових записів</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLibraryPage.qml" line="115"/>
        <source>Add Account</source>
        <translation>Додати обліковий запис</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="61"/>
        <source>Create a Local Library</source>
        <translation>Створіть локальну бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="69"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation>Локальна бібліотека зберігається виключно на вашому пристрої - це робить її ідеальною для стислого забезпечення конфіденційності!&lt;br/&gt;&lt;br/&gt;Використовуйте її, якщо хочете зберігати інформацію лише локально та регулярно створювати резервні копії всіх своїх даних за допомогою інших механізмів. Якщо вам потрібно отримати доступ до інформації на кількох пристроях, створіть натомість бібліотеку, яка синхронізується.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="79"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewLocalLibraryPage.qml" line="84"/>
        <source>My Local Library Name</source>
        <translation>Назва моєї локальної бібліотеки</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="19"/>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="71"/>
        <source>Connection Settings</source>
        <translation>Параметри Підключення</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="77"/>
        <source>Server Address:</source>
        <translation>Адреса сервера:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation>Логін</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="107"/>
        <source>Trouble Signing In?</source>
        <translation>Проблеми З Входом?</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="118"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation>Ми спробували відкрити ваш браузер, щоб увійти у ваш екземпляр NextCloud. Будь ласка, увійдіть і надайте доступ до OpenTodoList, щоб продовжити. Проблеми з доступом до NextCloud у браузері? Ви також можете вручну ввести своє ім&apos;я користувача та пароль.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="126"/>
        <source>Log in Manually</source>
        <translation>Увійдіть вручну</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="132"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation>В ідеалі ви використовуєте спеціальні паролі програм замість пароля користувача. Якщо ваш вхід захищено двофакторною автентифікацією (2FA), ви повинні використовувати спеціальні паролі для доступу до свого NextCloud. Ви можете створити такі паролі в налаштуваннях користувача.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="141"/>
        <source>Create App Password</source>
        <translation>Створіть пароль програми</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="150"/>
        <source>Account Settings</source>
        <translation>Налаштування аккаунта</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="160"/>
        <source>Copy Link</source>
        <translation>Копіювати посилання</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="166"/>
        <source>Copied!</source>
        <translation>Скопійовано!</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="175"/>
        <source>User:</source>
        <translation>Користувач:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="182"/>
        <source>User Name</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="189"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="196"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="206"/>
        <source>Disable Certificate Checks</source>
        <translation>Вимкнути перевірку сертифікатів</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="210"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="223"/>
        <source>Account Name</source>
        <translation>Ім&apos;я облікового запису</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewNextCloudAccountPage.qml" line="233"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Не вдалося підключитися до сервера. Будь ласка, перевірте ваше ім&apos;я користувача, пароль і адресу сервера та повторіть спробу.</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="52"/>
        <source>Note</source>
        <translation>Примітка </translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="41"/>
        <source>Todo List</source>
        <translation>Список справ</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="56"/>
        <source>Image</source>
        <translation>Зображення</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/NewTopLevelItemButton.qml" line="46"/>
        <source>Todo</source>
        <translation>Зробити</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="161"/>
        <source>Account Name</source>
        <translation>Ім&apos;я облікового запису</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="28"/>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="99"/>
        <source>Connection Settings</source>
        <translation>Параметри Підключення</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="105"/>
        <source>Server Address:</source>
        <translation>Адреса сервера:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="111"/>
        <source>https://myserver.example.com</source>
        <translation>https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="117"/>
        <source>User:</source>
        <translation>Користувач:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="123"/>
        <source>User Name</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="129"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="135"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="144"/>
        <source>Disable Certificate Checks</source>
        <translation>Вимкнути перевірку сертифікатів</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="148"/>
        <source>Name:</source>
        <translation>Ім&apos;я:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NewWebDAVAccountPage.qml" line="171"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation>Не вдалося підключитися до сервера. Будь ласка, перевірте ваше ім&apos;я користувача, пароль і адресу сервера та повторіть спробу.</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../qml/Widgets/NoteItem.qml" line="91"/>
        <source>Due on %1</source>
        <translation>Виконати до %1</translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="171"/>
        <source>Main Page</source>
        <translation>Головна сторінка</translation>
    </message>
    <message>
        <location filename="../qml/Pages/NotePage.qml" line="238"/>
        <source>New Page</source>
        <translation>Нова сторінка</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation>Системна мова</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation>Відсутні секрети для облікового запису</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="18"/>
        <location filename="../qml/Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation>Виявлені проблеми</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation>Не вдалося синхронізувати бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ProblemsPage.qml" line="91"/>
        <source>Retry Sync</source>
        <translation>Перепробувати Синхронізацію</translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../qml/Actions/PromoteTask.qml" line="13"/>
        <source>Promote</source>
        <translation>Просувати</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="312"/>
        <location filename="../appstartup.cpp" line="319"/>
        <source>unknown</source>
        <translation>невідомо</translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="17"/>
        <source>Quick Notes</source>
        <translation>Швидкі нотатки</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="36"/>
        <source>Quick Notes Editor</source>
        <translation>Редактор Швидких Нотаток</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="45"/>
        <source>Open the main window</source>
        <translation>Відкрити головне вікно</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="57"/>
        <source>Quick Note Title</source>
        <translation>Заголовок Швидкої Нотатки</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="121"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="126"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation>Збережіть введені нотатки до вибраної бібліотеки. Натисніть і утримуйте кнопку, щоб отримати більше варіантів збереження.</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="148"/>
        <source>Save as Note</source>
        <translation>Зберегти як Нотатку</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="161"/>
        <source>Save as Todo List</source>
        <translation>Зберегти як Список Завдань</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="165"/>
        <source>Quick Todo List</source>
        <translation>Швидкий Список Завдань</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="152"/>
        <source>Quick Note</source>
        <translation>Швидка Нотатка</translation>
    </message>
    <message>
        <location filename="../qml/Windows/QuickNoteWindow.qml" line="70"/>
        <source>Type your notes here...</source>
        <translation>Напишіть свої нотатки тут...</translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="29"/>
        <source>Edit Recurrence</source>
        <translation>Редагувати повторення</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation>Ніколи</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="47"/>
        <source>Daily</source>
        <translation>Щодня</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="51"/>
        <source>Weekly</source>
        <translation>Щотижня</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="55"/>
        <source>Monthly</source>
        <translation>Щомісяця</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="63"/>
        <source>Every N Days</source>
        <translation>Кожні N днів</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="97"/>
        <source>Recurs:</source>
        <translation>Повторюється:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="115"/>
        <source>Number of days:</source>
        <translation>Кількість днів:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="141"/>
        <source>Recur relative to the date when marking as done</source>
        <translation>Повторюється відносно дати під час позначення виконаного</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="59"/>
        <source>Yearly</source>
        <translation>Щорічно</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="67"/>
        <source>Every N Weeks</source>
        <translation>Кожен N Тижні</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="71"/>
        <source>Every N Months</source>
        <translation>Кожен N Місяці</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="117"/>
        <source>Number of weeks:</source>
        <translation>Кількість Тижнів:</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RecurrenceDialog.qml" line="119"/>
        <source>Number of months:</source>
        <translation>Кількість Місяців:</translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../qml/Actions/RenameItem.qml" line="10"/>
        <source>Rename</source>
        <translation>Перейменувати</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="21"/>
        <source>Rename Item</source>
        <translation>Перейменувати елемент</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameItemDialog.qml" line="32"/>
        <source>Enter item title...</source>
        <translation>Введіть назву заголовку...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="19"/>
        <source>Rename Library</source>
        <translation>Перейменувати бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Windows/RenameLibraryDialog.qml" line="36"/>
        <source>Enter library title...</source>
        <translation>Введіть назву бібліотеки...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../qml/Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation>Скинути Термін Виконання</translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="147"/>
        <source>Today</source>
        <translation>Сьогодні</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="148"/>
        <source>Tomorrow</source>
        <translation>Завтра</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="184"/>
        <source>Later This Week</source>
        <translation>Пізніше цього тижня</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="186"/>
        <source>Next Week</source>
        <translation>Наступного тижня</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="187"/>
        <source>Coming Next</source>
        <translation>Наступний</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="266"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation>Нічого не заплановано... Додайте термін виконання для елементів, щоб вони з’явилися тут.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="123"/>
        <source>Overdue</source>
        <translation>Прострочена</translation>
    </message>
    <message>
        <location filename="../qml/Pages/ScheduleViewPage.qml" line="52"/>
        <source>Schedule</source>
        <translation>Розпорядок</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../qml/Windows/SelectLibraryDialog.qml" line="21"/>
        <source>Select Library</source>
        <translation>Виберіть Бібліотеку</translation>
    </message>
</context>
<context>
    <name>SelectTodoDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="22"/>
        <source>Select Todo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="42"/>
        <source>Todo List:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Windows/SelectTodoDialog.qml" line="64"/>
        <source>Todo:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTodoListDialog.qml" line="24"/>
        <source>Select Todo List</source>
        <translation>Виберіть список завдань</translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../qml/Windows/SelectTopLevelItemDialog.qml" line="21"/>
        <source>Select Item</source>
        <translation>Виберіть Елемент</translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation>Заплановано на цей тиждень</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../qml/Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation>Запланувати наступний тиждень</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../qml/Actions/SetDueTo.qml" line="10"/>
        <source>Select Due Date</source>
        <translation>Виберіть Термін виконання</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../qml/Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation>Встановити термін сьогодні</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../qml/Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation>Термін виконання завтра</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../qml/Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation>Встановити прогрес</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="46"/>
        <source>User Interface</source>
        <translation>Інтерфейс користувача</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="54"/>
        <source>Language:</source>
        <translation>Мова:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="118"/>
        <source>Theme:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="208"/>
        <source>Font Size:</source>
        <translation>Розмір шрифту:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="225"/>
        <source>Use custom font size</source>
        <translation>Використати нестандартний розмір шрифту</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="243"/>
        <source>Use Compact Style</source>
        <translation>Використати компактний стиль</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="252"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation>Зменшіть простір між компонентами та зменшіть розмір шрифту.

&lt;em&gt;Потребує перезапуску додатка.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="260"/>
        <source>Use compact todo lists</source>
        <translation>Компактний вид списка</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="270"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation>Зменшіть відступи в списках справ і завдань, щоб вмістити більше елементів на екрані.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="277"/>
        <source>Override Scaling Factor</source>
        <translation>Перевизначити Коефіцієнт Масштабування</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="284"/>
        <source>Scale Factor:</source>
        <translation>Масштаб:</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="304"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation>Використовуйте для ручного масштабування інтерфейсу користувача. За замовчуванням програма автоматично адаптувується відповідно до конфігурації вашого пристрою. Якщо автоматичне налаштування не працює належним чином, можна встановити свій коефіцієнт масштабування.

Потребує перезапуску програми.</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="313"/>
        <source>Library Item Size:</source>
        <translation>Розмір Елемента Бібліотеки</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="189"/>
        <source>System Tray:</source>
        <translation>Системний Лоток</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="202"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation>Відкривати Редактор Швидких Нотаток за Кліком</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="234"/>
        <source>Desktop Mode</source>
        <translation>Режим Робочого Столу</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="329"/>
        <source>Show notes excerpt in listings</source>
        <translation>Показувати уривки нотаток у списках</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="194"/>
        <source>Monochrome Icon</source>
        <translation>Монохромна ікона</translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="143"/>
        <source>Custom Primary Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Pages/SettingsPage.qml" line="171"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Pages/SettingsPage.qml" line="165"/>
        <source>Custom Secondary Color:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StackViewWindow</name>
    <message>
        <location filename="../qml/Windows/StackViewWindow.qml" line="21"/>
        <source>OpenTodoList</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="45"/>
        <source>Libraries</source>
        <translation>Бібліотека</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="70"/>
        <source>Add a new library</source>
        <translation>Додати нову бібліотеку</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="89"/>
        <source>Accounts</source>
        <translation>Акаунти</translation>
    </message>
    <message>
        <location filename="../qml/Pages/StartPage.qml" line="111"/>
        <source>Add an account</source>
        <translation>Додати акаунт</translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="42"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation>Під час синхронізації бібліотеки виникли помилки. Переконайтеся, що налаштування бібліотеки актуальні.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="48"/>
        <source>Ignore</source>
        <translation>Ігнорувати</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/SyncErrorNotificationBar.qml" line="52"/>
        <source>View</source>
        <translation>Вигляд</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../qml/Widgets/TagsEditor.qml" line="32"/>
        <source>Add Tag</source>
        <translation>Додати Tag</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="136"/>
        <source>✔ No open todos - everything done</source>
        <translation>✔ Немає відкритих завдань – усе виконано</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/TodoListItem.qml" line="77"/>
        <source>Due on %1</source>
        <translation>Виконати до %1</translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="166"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Пошуковий термін 1, пошуковий термін 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="253"/>
        <source>Todos</source>
        <translation>Завдання</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="273"/>
        <source>Add new todo...</source>
        <translation>Додати нове завдання...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="178"/>
        <source>Manually</source>
        <translation>Вручну</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="184"/>
        <source>Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="190"/>
        <source>Due Date</source>
        <translation>Термін виконання</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="196"/>
        <source>Created At</source>
        <translation>Створено в</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="202"/>
        <source>Updated At</source>
        <translation>Оновлено в</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="215"/>
        <source>Show Completed</source>
        <translation>Показати завершені</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoListPage.qml" line="223"/>
        <source>Show At The End</source>
        <translation>Показувати В Кінці</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="181"/>
        <source>Search term 1, search term 2, ...</source>
        <translation>Пошуковий термін 1, пошуковий термін 2, ...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="206"/>
        <source>Tasks</source>
        <translation>Завдання</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="212"/>
        <source>Add new task...</source>
        <translation>Додати нове завдання...</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="303"/>
        <source>Show Completed</source>
        <translation>Показати завершені</translation>
    </message>
    <message>
        <location filename="../qml/Pages/TodoPage.qml" line="310"/>
        <source>Show At The End</source>
        <translation>Показувати В Кінці</translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../qml/Widgets/TodosWidget.qml" line="211"/>
        <source>Due on: %1</source>
        <translation>До: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../qml/Widgets/TodosWidgetDelegate.qml" line="238"/>
        <source>More Actions...</source>
        <translation>Більше дій</translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation>Доступне оновлення OpenTodoList %1.</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation>Ігнорувати</translation>
    </message>
    <message>
        <location filename="../qml/Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation>Завантажити</translation>
    </message>
</context>
</TS>
